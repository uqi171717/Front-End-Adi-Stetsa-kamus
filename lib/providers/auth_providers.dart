import 'dart:convert';

import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/kompetensi_model.dart';
import 'package:adistetsa/models/ktsp_model.dart';
import 'package:adistetsa/models/orangtua_model.dart';
import 'package:adistetsa/models/pekanaktif_model.dart';
import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/models/silabusrpb_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/models/token_model.dart';

import 'package:adistetsa/services/auth_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AuthProvider with ChangeNotifier {
  String baseUrl = 'https://adistetsa.pythonanywhere.com';
  late TokenModel _token;

  SiswaModel _siswa = SiswaModel();
  GuruModel _guru = GuruModel();
  OrangTuaModel _orangTua = OrangTuaModel();
  KaryawanModel _karyawan = KaryawanModel();
  SilabusRPBModel _silabusRPB = SilabusRPBModel();
  KTSPModel _ktsp = KTSPModel();
  PekanAktifModel _pekanaktif = PekanAktifModel();
  late RolesModel _role;
  List listTahunAjaran = [];
  List listMataPelajaran = [];
  List listKelas = [];
  List listSemester = [];
  List listKelasSiswa = [];

  //NOTE : GET MODEL
  // ignore: non_constant_identifier_names
  TokenModel get Token => _token;
  // ignore: non_constant_identifier_names
  SiswaModel get Siswa => _siswa;
  // ignore: non_constant_identifier_names
  GuruModel get Guru => _guru;
  // ignore: non_constant_identifier_names
  OrangTuaModel get OrangTua => _orangTua;
  // ignore: non_constant_identifier_names
  KaryawanModel get Karyawan => _karyawan;
  // ignore: non_constant_identifier_names
  SilabusRPBModel get SilabusRPB => _silabusRPB;
  // ignore: non_constant_identifier_names
  RolesModel get Role => _role;
  // ignore: non_constant_identifier_names
  KTSPModel get KTSP => _ktsp;
  // ignore: non_constant_identifier_names
  PekanAktifModel get PekanAktif => _pekanaktif;

  //NOTE : SET DATA
  set token(TokenModel token) {
    _token = token;
    notifyListeners();
  }

  set siswa(SiswaModel siswa) {
    _siswa = siswa;
    notifyListeners();
  }

  set guru(GuruModel guru) {
    _guru = guru;
    notifyListeners();
  }

  set orangTua(OrangTuaModel orangTua) {
    _orangTua = orangTua;
    notifyListeners();
  }

  set karyawan(KaryawanModel karyawan) {
    _karyawan = karyawan;
    notifyListeners();
  }

  set silabus(SilabusRPBModel silabus) {
    _silabusRPB = silabus;
    notifyListeners();
  }

  set role(RolesModel role) {
    _role = role;
    notifyListeners();
  }

  set ktsp(KTSPModel ktsp) {
    _ktsp = ktsp;
    notifyListeners();
  }

  set pekanaktif(PekanAktifModel pekanAktif) {
    _pekanaktif = pekanAktif;
    notifyListeners();
  }

  //NOTE : POST DATA
  Future<bool> login({
    String? username,
    String? password,
  }) async {
    try {
      TokenModel access = await AuthService().login(
        username: username,
        password: password,
      );

      _token = access;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  //NOTE : PATCH
  Future<bool> editProfile({
    String email = '',
    String noHp = '',
  }) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var role = prefs.getString('role');
      if (email != '') {
        if (role == 'Siswa') {
          SiswaModel siswaModel = await AuthService().editProfile(email: email);
          _siswa = siswaModel;
        } else if (role == 'Guru') {
          GuruModel guruModel = await AuthService().editProfile(email: email);
          _guru = guruModel;
        } else if (role == 'Karyawan') {
          KaryawanModel karyawanModel =
              await AuthService().editProfile(email: email);
          _karyawan = karyawanModel;
        } else {
          print("Tidak ada");
        }
      } else if (noHp != '') {
        if (role == 'Siswa') {
          SiswaModel siswaModel = await AuthService().editProfile(noHp: noHp);
          _siswa = siswaModel;
        } else if (role == 'Guru') {
          GuruModel guruModel = await AuthService().editProfile(noHp: noHp);
          _guru = guruModel;
        } else if (role == 'Karyawan') {
          KaryawanModel karyawanModel =
              await AuthService().editProfile(noHp: noHp);
          _karyawan = karyawanModel;
        } else {
          print("Tidak ada");
        }
      }
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  //NOTE : GET DATA
  getRoles() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/daftar_role');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body);
      List<RolesModel> roles =
          data.map((item) => RolesModel.fromJson(item)).toList();
      return roles;
    } else {
      throw Exception('Gagal Mendapatkan roles');
    }
  }

  getKompetensiGuru() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/data_guru_kompetensi');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      List<KompetensiModel> roles =
          data.map((item) => KompetensiModel.fromJson(item)).toList();
      return roles;
    } else {
      throw Exception('Gagal Login');
    }
  }

  deleteSilabus({
    int? id,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/silabus_rpb/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.delete(url, headers: headers);
    if (response.statusCode == 200 || response.statusCode == 204) {
      print('Berhasil');
      return true;
    } else {
      return false;
    }
  }

  deleteKTSP({
    int? id,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/ktsp/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.delete(url, headers: headers);
    if (response.statusCode == 200 || response.statusCode == 204) {
      print('Berhasil');
      return true;
    } else {
      return false;
    }
  }

  getDataAnak() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/profile');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      OrangTuaModel orangTua = OrangTuaModel.fromJson(data);
      return orangTua;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  getMataPelajaran() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/mata_pelajaran');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      List<KTSPModel> ktspModel =
          data.map((item) => KTSPModel.fromJson(item)).toList();
      return ktspModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  getKTSP(String? urlFilter) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/ktsp?$urlFilter');
    print(url);
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      List<KTSPModel> ktspModel =
          data.map((item) => KTSPModel.fromJson(item)).toList();
      return ktspModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  Future<bool> getSiswaProfile() async {
    try {
      SiswaModel siswaModel = await AuthService().getProfile();
      _siswa = siswaModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getGuruProfile() async {
    try {
      GuruModel guruModel = await AuthService().getProfile();
      _guru = guruModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getPekanAktif() async {
    try {
      PekanAktifModel pekanAktifModel = await AuthService().getPekanAktif();
      _pekanaktif = pekanAktifModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getKTSPID({int? id}) async {
    try {
      KTSPModel ktspModel = await AuthService().getKTSPID(id: id);
      _ktsp = ktspModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getDataSiswaNIS({String? nis}) async {
    try {
      SiswaModel siswaModel = await AuthService().getDataSiswaNIS(nis: nis);
      _siswa = siswaModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getSilabusRPBID({int? id}) async {
    try {
      SilabusRPBModel silabusRPBModel =
          await AuthService().geSilabusRPBID(id: id);
      _silabusRPB = silabusRPBModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getOrangtuaProfile() async {
    try {
      OrangTuaModel orangTua = await AuthService().getProfile();
      _orangTua = orangTua;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getKaryawanProfile() async {
    try {
      KaryawanModel karyawanModel = await AuthService().getProfile();
      _karyawan = karyawanModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> getSilabus(String? url) async {
    try {
      await AuthService().getSilabusRPB(url);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> getTahunAjaranFilter(BuildContext context) async {
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/tahun_ajaran');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    notifyListeners();
    if (response.statusCode == 200) {
      var dataRiwayat = jsonDecode(response.body);
      listTahunAjaran = dataRiwayat['results'];
      print(listTahunAjaran);
      notifyListeners();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error Get Data')));
    }
  }

  Future<void> getMataPelajaranFilter(BuildContext context) async {
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/mata_pelajaran');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    notifyListeners();
    if (response.statusCode == 200) {
      var dataRiwayat = jsonDecode(response.body);
      listMataPelajaran = dataRiwayat['results'];
      print(listTahunAjaran);
      notifyListeners();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error Get Data')));
    }
  }

  Future<void> getKelasFilter(BuildContext context) async {
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/kelas');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    notifyListeners();
    if (response.statusCode == 200) {
      var dataRiwayat = jsonDecode(response.body);
      listKelas = dataRiwayat['results'];
      print(listTahunAjaran);
      notifyListeners();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error Get Data')));
    }
  }

  Future<void> getSemesterFilter(BuildContext context) async {
    notifyListeners();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/semester');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    notifyListeners();
    if (response.statusCode == 200) {
      var dataRiwayat = jsonDecode(response.body);
      listSemester = dataRiwayat['results'];
      print(listTahunAjaran);
      notifyListeners();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error Get Data')));
    }
  }

  tambahKTSP(int? id, filepath) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/ktsp');
    var request = http.MultipartRequest('POST', url);
    request.headers['Authorization'] = token;
    request.fields['TAHUN_AJARAN'] = id.toString();
    request.files.add(await http.MultipartFile.fromPath('NAMA_FILE', filepath));
    var response = await request.send();
    final res = await http.Response.fromStream(response);
    print(res.statusCode);
    if (res.statusCode == 500) {
      return false;
    }
    return true;
  }

  editKTSP(int? id, int? tahunajaran, filepath) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/ktsp/$id');
    var request = http.MultipartRequest('PATCH', url);
    request.headers['Authorization'] = token;
    request.fields['TAHUN_AJARAN'] = tahunajaran.toString();
    if (filepath != null) {
      request.files
          .add(await http.MultipartFile.fromPath('NAMA_FILE', filepath));
      var response = await request.send();
      final res = await http.Response.fromStream(response);
      print(res.statusCode);
      if (res.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } else {
      var response = await request.send();
      final res = await http.Response.fromStream(response);
      print(res.statusCode);

      if (res.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    }
  }

  tambahSilabusRPB(String? mataPelajaran, int? tahunAjaran, int? kelas,
      int? semester, filepath) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/silabus_rpb');
    var request = http.MultipartRequest('POST', url);
    request.headers['Authorization'] = token;
    request.fields['MATA_PELAJARAN'] = mataPelajaran.toString();
    request.fields['TAHUN_AJARAN'] = tahunAjaran.toString();
    request.fields['KELAS'] = kelas.toString();
    request.fields['SEMESTER'] = semester.toString();
    request.files.add(await http.MultipartFile.fromPath('NAMA_FILE', filepath));
    var response = await request.send();
    final res = await http.Response.fromStream(response);
    if (res.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  editSilabusRPB(int? id, String? mataPelajaran, int? tahunAjaran, int? kelas,
      int? semester, filepath) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/silabus_rpb/$id');
    var request = http.MultipartRequest('PATCH', url);
    request.headers['Authorization'] = token;
    request.fields['MATA_PELAJARAN'] = mataPelajaran.toString();
    request.fields['TAHUN_AJARAN'] = tahunAjaran.toString();
    request.fields['KELAS'] = kelas.toString();
    request.fields['SEMESTER'] = semester.toString();
    if (filepath != null) {
      request.files
          .add(await http.MultipartFile.fromPath('NAMA_FILE', filepath));
      var response = await request.send();
      final res = await http.Response.fromStream(response);
      if (res.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } else {
      var response = await request.send();
      final res = await http.Response.fromStream(response);
      if (res.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    }
  }

  getDataSiswa() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/data_siswa');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      // print(data);
      List<SiswaModel> siswaModel =
          data.map((item) => SiswaModel.fromJson(item)).toList();
      return siswaModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  Future<void> getKelasSiswa(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token").toString();
    var url = Uri.parse('$baseUrl/kurikulum/kelas_siswa');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    // print(response.body);
    if (response.statusCode == 200) {
      var dataRiwayat = jsonDecode(response.body);
      listKelasSiswa = dataRiwayat['results'];
      // print(listKelasSiswa);
      notifyListeners();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error Get Data')));
    }
  }
}

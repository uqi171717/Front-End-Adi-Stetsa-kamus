class RolesModel {
  String name = '';

  RolesModel({
    required this.name,
  });

  // Mengambil data json model
  factory RolesModel.fromJson(Map<String, dynamic> json) {
    return RolesModel(
      name: json['name'],
    );
  }
}

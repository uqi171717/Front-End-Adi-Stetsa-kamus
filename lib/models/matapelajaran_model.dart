class MataPelajaranModel {
  String? kODE;
  String? nAMA;

  MataPelajaranModel({this.kODE, this.nAMA});

  MataPelajaranModel.fromJson(Map<String, dynamic> json) {
    kODE = json['KODE'];
    nAMA = json['NAMA'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['KODE'] = this.kODE;
    data['NAMA'] = this.nAMA;
    return data;
  }
}

class UserModel {
  String? nis;
  String? nama;
  String? jenisKelamin;
  String? agama;
  String? token;

  UserModel({
    required this.nis,
    required this.nama,
    required this.jenisKelamin,
    required this.agama,
    required this.token,
  });

  // NOTE: Mengambil Json
  UserModel.fromJson(Map<String, dynamic> json) {
    nis = json['NIS'];
    nama = json['NAMA'];
    jenisKelamin = json['JENIS_KELAMIN'];
    agama = json['AGAMA'];
    token = json['token'];
  }

  // NOTE: Mengirim data json
  Map<String, dynamic> toJson() {
    return {
      'NIS': nis,
      'NAMA': nama,
      'JENIS_KELAMIN': jenisKelamin,
      'AGAMA': agama,
      'token': token,
    };
  }
}

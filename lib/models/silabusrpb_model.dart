class SilabusRPBModel {
  int? iD;
  var nAMAFILE;
  String? mATAPELAJARAN;
  var tAHUNAJARAN;
  var kELAS;
  var sEMESTER;

  SilabusRPBModel(
      {this.iD,
      this.nAMAFILE,
      this.mATAPELAJARAN,
      this.tAHUNAJARAN,
      this.kELAS,
      this.sEMESTER});

  SilabusRPBModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    nAMAFILE = json['NAMA_FILE'];
    mATAPELAJARAN = json['MATA_PELAJARAN'];
    tAHUNAJARAN = json['TAHUN_AJARAN'];
    kELAS = json['KELAS'];
    sEMESTER = json['SEMESTER'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['NAMA_FILE'] = this.nAMAFILE;
    data['MATA_PELAJARAN'] = this.mATAPELAJARAN;
    data['TAHUN_AJARAN'] = this.tAHUNAJARAN;
    data['KELAS'] = this.kELAS;
    data['SEMESTER'] = this.sEMESTER;
    return data;
  }
}

class KTSPModel {
  int? iD;
  String? nAMAFILE;
  var tAHUNAJARAN;

  KTSPModel({this.iD, this.nAMAFILE, this.tAHUNAJARAN});

  KTSPModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    nAMAFILE = json['NAMA_FILE'];
    tAHUNAJARAN = json['TAHUN_AJARAN'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['NAMA_FILE'] = this.nAMAFILE;
    data['TAHUN_AJARAN'] = this.tAHUNAJARAN;
    return data;
  }
}

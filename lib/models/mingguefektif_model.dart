class MingguEfektifModel {
  String? bULAN;
  int? jUMLAHMINGGU;
  int? jUMLAHMINGGUEFEKTIF;
  int? jUMLAHMINGGUTIDAKEFEKTIF;
  String? kETERANGAN;

  MingguEfektifModel(
      {this.bULAN,
      this.jUMLAHMINGGU,
      this.jUMLAHMINGGUEFEKTIF,
      this.jUMLAHMINGGUTIDAKEFEKTIF,
      this.kETERANGAN});

  MingguEfektifModel.fromJson(Map<String, dynamic> json) {
    bULAN = json['BULAN'];
    jUMLAHMINGGU = json['JUMLAH_MINGGU'];
    jUMLAHMINGGUEFEKTIF = json['JUMLAH_MINGGU_EFEKTIF'];
    jUMLAHMINGGUTIDAKEFEKTIF = json['JUMLAH_MINGGU_TIDAK_EFEKTIF'];
    kETERANGAN = json['KETERANGAN'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BULAN'] = this.bULAN;
    data['JUMLAH_MINGGU'] = this.jUMLAHMINGGU;
    data['JUMLAH_MINGGU_EFEKTIF'] = this.jUMLAHMINGGUEFEKTIF;
    data['JUMLAH_MINGGU_TIDAK_EFEKTIF'] = this.jUMLAHMINGGUTIDAKEFEKTIF;
    data['KETERANGAN'] = this.kETERANGAN;
    return data;
  }
}

class OrangTuaModel {
  int? iD;
  String? nIKAYAH;
  String? nAMAAYAH;
  String? tAHUNLAHIRAYAH;
  String? jENJANGPENDIDIKANAYAH;
  String? pEKERJAANAYAH;
  String? pENGHASILANAYAH;
  String? nIKIBU;
  String? nAMAIBU;
  String? tAHUNLAHIRIBU;
  String? jENJANGPENDIDIKANIBU;
  String? pEKERJAANIBU;
  String? pENGHASILANIBU;
  String? nIKWALI;
  String? nAMAWALI;
  String? tAHUNLAHIRWALI;
  String? jENJANGPENDIDIKANWALI;
  String? pEKERJAANWALI;
  String? pENGHASILANWALI;
  List<String>? dATAANAK;

  OrangTuaModel(
      {this.iD,
      this.nIKAYAH,
      this.nAMAAYAH,
      this.tAHUNLAHIRAYAH,
      this.jENJANGPENDIDIKANAYAH,
      this.pEKERJAANAYAH,
      this.pENGHASILANAYAH,
      this.nIKIBU,
      this.nAMAIBU,
      this.tAHUNLAHIRIBU,
      this.jENJANGPENDIDIKANIBU,
      this.pEKERJAANIBU,
      this.pENGHASILANIBU,
      this.nIKWALI,
      this.nAMAWALI,
      this.tAHUNLAHIRWALI,
      this.jENJANGPENDIDIKANWALI,
      this.pEKERJAANWALI,
      this.pENGHASILANWALI,
      this.dATAANAK});

  OrangTuaModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    nIKAYAH = json['NIK_AYAH'];
    nAMAAYAH = json['NAMA_AYAH'];
    tAHUNLAHIRAYAH = json['TAHUN_LAHIR_AYAH'];
    jENJANGPENDIDIKANAYAH = json['JENJANG_PENDIDIKAN_AYAH'];
    pEKERJAANAYAH = json['PEKERJAAN_AYAH'];
    pENGHASILANAYAH = json['PENGHASILAN_AYAH'];
    nIKIBU = json['NIK_IBU'];
    nAMAIBU = json['NAMA_IBU'];
    tAHUNLAHIRIBU = json['TAHUN_LAHIR_IBU'];
    jENJANGPENDIDIKANIBU = json['JENJANG_PENDIDIKAN_IBU'];
    pEKERJAANIBU = json['PEKERJAAN_IBU'];
    pENGHASILANIBU = json['PENGHASILAN_IBU'];
    nIKWALI = json['NIK_WALI'];
    nAMAWALI = json['NAMA_WALI'];
    tAHUNLAHIRWALI = json['TAHUN_LAHIR_WALI'];
    jENJANGPENDIDIKANWALI = json['JENJANG_PENDIDIKAN_WALI'];
    pEKERJAANWALI = json['PEKERJAAN_WALI'];
    pENGHASILANWALI = json['PENGHASILAN_WALI'];
    dATAANAK = json['DATA_ANAK'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['NIK_AYAH'] = this.nIKAYAH;
    data['NAMA_AYAH'] = this.nAMAAYAH;
    data['TAHUN_LAHIR_AYAH'] = this.tAHUNLAHIRAYAH;
    data['JENJANG_PENDIDIKAN_AYAH'] = this.jENJANGPENDIDIKANAYAH;
    data['PEKERJAAN_AYAH'] = this.pEKERJAANAYAH;
    data['PENGHASILAN_AYAH'] = this.pENGHASILANAYAH;
    data['NIK_IBU'] = this.nIKIBU;
    data['NAMA_IBU'] = this.nAMAIBU;
    data['TAHUN_LAHIR_IBU'] = this.tAHUNLAHIRIBU;
    data['JENJANG_PENDIDIKAN_IBU'] = this.jENJANGPENDIDIKANIBU;
    data['PEKERJAAN_IBU'] = this.pEKERJAANIBU;
    data['PENGHASILAN_IBU'] = this.pENGHASILANIBU;
    data['NIK_WALI'] = this.nIKWALI;
    data['NAMA_WALI'] = this.nAMAWALI;
    data['TAHUN_LAHIR_WALI'] = this.tAHUNLAHIRWALI;
    data['JENJANG_PENDIDIKAN_WALI'] = this.jENJANGPENDIDIKANWALI;
    data['PEKERJAAN_WALI'] = this.pEKERJAANWALI;
    data['PENGHASILAN_WALI'] = this.pENGHASILANWALI;
    data['DATA_ANAK'] = this.dATAANAK;
    return data;
  }
}

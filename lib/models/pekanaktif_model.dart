class PekanAktifModel {
  int? iD;
  String? mATAPELAJARAN;
  String? kELAS;
  String? sEMESTER;

  PekanAktifModel({this.iD, this.mATAPELAJARAN, this.kELAS, this.sEMESTER});

  PekanAktifModel.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    mATAPELAJARAN = json['MATA_PELAJARAN'];
    kELAS = json['KELAS'];
    sEMESTER = json['SEMESTER'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['MATA_PELAJARAN'] = this.mATAPELAJARAN;
    data['KELAS'] = this.kELAS;
    data['SEMESTER'] = this.sEMESTER;
    return data;
  }
}

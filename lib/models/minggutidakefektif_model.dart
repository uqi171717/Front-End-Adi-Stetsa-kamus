class MingguTidakEfektifModel {
  String? uRAIANKEGIATAN;
  int? jUMLAHMINGGU;
  String? kETERANGAN;

  MingguTidakEfektifModel(
      {this.uRAIANKEGIATAN, this.jUMLAHMINGGU, this.kETERANGAN});

  MingguTidakEfektifModel.fromJson(Map<String, dynamic> json) {
    uRAIANKEGIATAN = json['URAIAN_KEGIATAN'];
    jUMLAHMINGGU = json['JUMLAH_MINGGU'];
    kETERANGAN = json['KETERANGAN'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['URAIAN_KEGIATAN'] = this.uRAIANKEGIATAN;
    data['JUMLAH_MINGGU'] = this.jUMLAHMINGGU;
    data['KETERANGAN'] = this.kETERANGAN;
    return data;
  }
}

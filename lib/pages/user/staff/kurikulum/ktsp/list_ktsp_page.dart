import 'package:adistetsa/models/ktsp_model.dart';

import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/theme.dart';
import 'package:adistetsa/widget/loading.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ListKtspPagePage extends StatefulWidget {
  @override
  _ListKtspPagePageState createState() => _ListKtspPagePageState();
}

class _ListKtspPagePageState extends State<ListKtspPagePage> {
  Object? valueItem1;

  int flag1 = 0;
  bool isLoading = false;
  bool isSearch = false;
  String urlFilter = '';
  TextEditingController searchController = TextEditingController();
  String urlSearch = '';
  String baseURL = '';

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    setState(() {
      baseURL = '$urlSearch$urlFilter';
    });
    PreferredSizeWidget searchAppbar() {
      return AppBar(
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: true,
        leading: GestureDetector(
          onTap: () async {
            setState(() {
              searchController.clear();
              urlSearch = '';
              isSearch = false;
              isLoading = true;
            });
            await authProvider.getKTSP(urlFilter);
            setState(() {
              isLoading = false;
            });
          },
          child: Icon(
            Icons.arrow_back,
            color: wir1Color,
          ),
        ),
        title: TextFormField(
          controller: searchController,
          decoration: InputDecoration(
            hintText: 'Search',
            isDense: true,
            border: InputBorder.none,
          ),
          onChanged: (newValue) async {
            setState(() {
              if (searchController.selection.start >
                  searchController.text.length) {
                searchController.selection = new TextSelection.fromPosition(
                    new TextPosition(offset: searchController.text.length));
                searchController.text = newValue.toString();
              }
              print(searchController.text);
              urlSearch = 'search=${searchController.text}';
              isLoading = true;
            });
            await authProvider.getKTSP(urlFilter);
            setState(() {
              isLoading = false;
            });
          },
        ),
        elevation: 4,
        centerTitle: false,
      );
    }

    PreferredSizeWidget ktspAppbar() {
      return AppBar(
        title: Text(
          'KTSP',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pushNamedAndRemoveUntil(
                context, '/main-page/back', (route) => false);
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: wir1Color,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  isSearch = true;
                });
              },
              child: Icon(
                Icons.search,
                color: wir1Color,
              ),
            ),
          ),
        ],
        elevation: 4,
        centerTitle: true,
      );
    }

    launchUrl({required String url}) async {
      launch(
        url,
      );
    }

    downloadModal(String? url) async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: whiteTextStyleColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 20,
              ),
              width: 305,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Text(
                          'Notifikasi',
                          style: buttonTextStyle.copyWith(
                            color: wir1Color,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/cancel_button.png',
                            width: 14,
                            height: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Klik tombol di bawah untuk mengunduh file',
                    style: subtitleProfileTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  TextButton(
                    onPressed: () {
                      launchUrl(url: url.toString());
                      Navigator.pop(context);
                    },
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 6,
                      ),
                      backgroundColor: m1Color,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/user/icon_download.png',
                          width: 11,
                          height: 11,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Download',
                          style: labelTextStyle.copyWith(
                            color: whiteTextStyleColor,
                            fontWeight: semiBold,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    deleteModal(int? id) async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: whiteTextStyleColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 20,
              ),
              width: 305,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Text(
                          'Hapus File',
                          style: buttonTextStyle.copyWith(
                            color: wir1Color,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/cancel_button.png',
                            width: 14,
                            height: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Klik tombol di bawah untuk menghapus file',
                    style: subtitleProfileTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: mono2Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Tidak',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      TextButton(
                        onPressed: () async {
                          setState(() {
                            isLoading = true;
                          });
                          Navigator.pop(context);
                          await authProvider.deleteKTSP(id: id);
                          setState(() {
                            isLoading = false;
                          });
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: d1Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Hapus',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    Widget expandList({
      required String url,
      required String header,
      required int? id,
    }) {
      return ExpandableNotifier(
          child: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          children: [
            ExpandablePanel(
              theme: const ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                tapBodyToCollapse: true,
              ),
              header: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    header,
                    style: heading2TextStyle.copyWith(
                      color: wir1Color,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
              collapsed: Container(),
              expanded: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // children: content.map((item) {
                    //   return Text(
                    //     item.toString(),
                    //     style: labelTextStyle,
                    //     softWrap: true,
                    //     overflow: TextOverflow.fade,
                    //   );
                    // }).toList(),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    children: [
                      TextButton(
                        onPressed: () {
                          downloadModal(url);
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: s4Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/user/icon_download.png',
                              width: 11,
                              height: 11,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Download',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      OutlinedButton(
                        onPressed: () async {
                          loading(context);
                          if (await authProvider.getKTSPID(id: id)) {
                            Navigator.pushReplacementNamed(
                                context, '/staff/ktsp/edit/');
                          }
                        },
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                            color: mono2Color,
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: Colors.transparent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/user/icon_edit.png',
                              width: 11,
                              height: 11,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Edit',
                              style: labelTextStyle.copyWith(
                                color: mono2Color,
                                fontWeight: semiBold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: 38,
                        child: TextButton(
                          onPressed: () {
                            deleteModal(id);
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: d1Color,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                          child: Image.asset(
                            'assets/user/icon_delete.png',
                            width: 14,
                            height: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 0.5,
            ),
          ],
        ),
      ));
    }

    Widget dropdownList1({required String hint}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag1 == 1 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () async {
                setState(() {
                  isLoading = true;
                  urlFilter = '';
                  flag1 = 0;
                  valueItem1 = null;
                });
                await authProvider.getTahunAjaranFilter(context);
                setState(() {
                  isLoading = false;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag1 == 1 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag1 == 1 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: valueItem1,
                items: authProvider.listTahunAjaran.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['ID'],
                      child: Text(
                        value['tahun_ajaran'],
                        style: labelTextStyle.copyWith(
                          color: flag1 == 1 ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) async {
                  setState(() {
                    isLoading = true;
                    flag1 = 1;
                    valueItem1 = value;
                    urlFilter = '&TAHUN_AJARAN=$value';
                    print(urlFilter);
                  });
                  await authProvider.getTahunAjaranFilter(context);

                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ),
          ));
    }

    Widget filter() {
      return Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    // NOTE: ini adalah button reset yang hanya akan muncul jika salah satu dropdown dipilih
                    flag1 == 1
                        ? GestureDetector(
                            onTap: () async {
                              setState(() {
                                isLoading = true;
                                valueItem1 = null;
                                urlFilter = '';
                                flag1 = 0;
                              });
                              await authProvider.getTahunAjaranFilter(context);
                              setState(() {
                                isLoading = false;
                              });
                            },
                            child: Container(
                              height: 24,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: s4Color,
                              ),
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/user/reset_icon.png',
                                    height: 8,
                                    width: 8,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    'reset',
                                    style: labelTextStyle.copyWith(
                                      color: whiteTextStyleColor,
                                      fontWeight: semiBold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),

                    dropdownList1(
                      hint: 'Tahun Ajaran',
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }

    setState(() {
      baseURL = '$urlSearch$urlFilter';
    });

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushNamedAndRemoveUntil(
            context, '/main-page/back', (route) => false);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            authProvider.getTahunAjaranFilter(context);
            Navigator.pushNamed(context, '/staff/ktsp/tambah/');
          },
          tooltip: 'Tambah Silabus RPB',
          child: const Icon(
            Icons.add,
            size: 27.44,
          ),
          backgroundColor: s3Color,
        ),
        appBar: isSearch == false ? ktspAppbar() : searchAppbar(),
        body: Column(
          children: [
            filter(),
            Expanded(
              child: isLoading == false
                  ? ListView(
                      children: [
                        FutureBuilder(
                          future: authProvider.getKTSP(baseURL),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              List<KTSPModel> data = snapshot.data;
                              return data.isEmpty
                                  ? Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.81,
                                      child: Center(
                                        child: Text(
                                          'Data tidak ditemukan',
                                          style: labelTextStyle.copyWith(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                    )
                                  : Column(
                                      children: data.map((item) {
                                      return expandList(
                                        url: item.nAMAFILE.toString(),
                                        id: item.iD,
                                        header: item.tAHUNAJARAN.toString(),
                                      );
                                    }).toList());
                            } else {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.81,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: Colors.white,
                                        backgroundColor: s1Color,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        )
                      ],
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}

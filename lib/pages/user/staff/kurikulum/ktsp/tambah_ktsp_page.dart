import 'package:adistetsa/providers/auth_providers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class TambahKtspPage extends StatefulWidget {
  const TambahKtspPage({Key? key}) : super(key: key);

  @override
  _TambahKtspPageState createState() => _TambahKtspPageState();
}

Object? value1Item;

bool isActive1 = false;
PlatformFile? file;
FilePickerResult? result;
bool isLoading = false;

class _TambahKtspPageState extends State<TambahKtspPage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    _selectFolder() async {
      result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx'],
      );
      if (result != null) {
        setState(() {
          file = result!.files.first;
        });
      } else {}
    }

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 4,
        backgroundColor: whiteTextStyleColor,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            value1Item = null;
            isLoading = false;
            isActive1 = false;
            file = null;
            result = null;
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
          ),
        ),
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        title: Text(
          'Tambah KTSP',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dropdownInput1({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive1 == false ? mono3Color : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value1Item = null;
                    isActive1 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive1 == false ? mono3Color : m1Color,
                  ),
                  value: value1Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listTahunAjaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['tahun_ajaran'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value1Item != value['ID']
                                  ? wir1Color
                                  : m1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive1 = true;
                      value1Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonFile() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'File',
            style: labelTextStyle.copyWith(
              color: wir1Color,
            ),
          ),
          TextButton(
            onPressed: () {
              _selectFolder();
            },
            style: TextButton.styleFrom(
              backgroundColor: s4Color,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/user/icon_upload.png',
                  width: 11,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    (file == null) ? 'Pilih File' : file!.name.toString(),
                    style: footerTextStyle.copyWith(
                      color: whiteTextStyleColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget buttonSubmit() {
      return Container(
        height: 46,
        margin: EdgeInsets.symmetric(
          vertical: 40,
        ),
        width: double.infinity,
        child: TextButton(
          onPressed: () async {
            if (isActive1 == true && file != null) {
              if (file!.extension == 'DOC' ||
                  file!.extension == 'DOCX' ||
                  file!.extension == 'pdf') {
                setState(() {
                  isLoading = true;
                });
                if (await authProvider.tambahKTSP(
                    int.parse(value1Item.toString()), file!.path)) {
                  setState(() {
                    value1Item = null;

                    isActive1 = false;
                    file = null;
                    result = null;
                    isLoading = false;
                  });
                  Navigator.pushNamed(context, '/staff/ktsp/back');
                } else {
                  setState(() {
                    value1Item = null;

                    isActive1 = false;
                    file = null;
                    result = null;
                    isLoading = false;
                  });
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    backgroundColor: d1Color,
                    content: Text(
                      "Gagal Menambahkan Data (1 tahun untuk 1 data KTSP)",
                      textAlign: TextAlign.center,
                    ),
                  ));
                }
              } else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  backgroundColor: d1Color,
                  content: Text(
                    "Format File tidak didukung",
                    textAlign: TextAlign.center,
                  ),
                ));
              }
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: d1Color,
                content: Text(
                  "Anda belum mengisi semua data yang diperlukan",
                  textAlign: TextAlign.center,
                ),
              ));
            }
            setState(() {
              isLoading = false;
            });
          },
          style: TextButton.styleFrom(
              backgroundColor: m1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              )),
          child: isLoading == false
              ? Text(
                  'Simpan',
                  style: buttonTextStyle.copyWith(
                    fontWeight: bold,
                    color: whiteTextStyleColor,
                  ),
                )
              : Container(
                  width: 14,
                  height: 14,
                  child: CircularProgressIndicator(
                    color: whiteTextStyleColor,
                    strokeWidth: 4,
                  ),
                ),
        ),
      );
    }

    Widget content() {
      return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          padding: EdgeInsets.only(
            top: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              dropdownInput1(
                title: 'Tahun Ajaran',
                hint: 'Tahun Ajaran',
              ),
              buttonFile(),
              buttonSubmit(),
            ],
          ),
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        value1Item = null;
        isLoading = false;
        isActive1 = false;
        file = null;
        result = null;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

import 'package:adistetsa/models/ktsp_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class EditKtspPage extends StatefulWidget {
  EditKtspPage({Key? key}) : super(key: key);

  @override
  _EditKtspPageState createState() => _EditKtspPageState();
}

Object? value1Item;

bool isActive1 = false;
PlatformFile? file;
FilePickerResult? result;
bool isLoading = false;

class _EditKtspPageState extends State<EditKtspPage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    KTSPModel ktspModel = authProvider.KTSP;
    String link = ktspModel.nAMAFILE.toString();
    String delimiter = 'KTSP/';
    int lastIndex = link.indexOf(delimiter);

    String trimmed = link.substring(lastIndex + 5, link.length);

    print(ktspModel.tAHUNAJARAN);

    _selectFolder() async {
      result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'doc', 'docx'],
      );
      if (result != null) {
        setState(() {
          file = result!.files.first;
        });
      } else {}
    }

    handleEdit() async {
      setState(() {
        isLoading = true;
      });
      if (await authProvider.editKTSP(ktspModel.iD,
          int.parse(value1Item.toString()), file == null ? null : file!.path)) {
        if (file != null) {
          if (file!.extension == 'DOC' ||
              file!.extension == 'DOCX' ||
              file!.extension == 'pdf') {
            value1Item = null;
            isActive1 = false;
            file = null;
            result = null;
            isLoading = false;
            Navigator.pushNamed(context, '/staff/ktsp/back');
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: d1Color,
              content: Text(
                "Format File tidak didukung",
                textAlign: TextAlign.center,
              ),
            ));
          }
        }
        value1Item = null;
        isActive1 = false;
        file = null;
        result = null;
        isLoading = false;
        Navigator.pushNamed(context, '/staff/ktsp/back');
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: d1Color,
          content: Text(
            "Gagal Melakukan Update (1 tahun untuk 1 data KTSP)",
            textAlign: TextAlign.center,
          ),
        ));
      }
      setState(() {
        isLoading = false;
      });
    }

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 2,
        backgroundColor: whiteTextStyleColor,
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
            onTap: () {
              value1Item = null;
              isActive1 = false;
              file = null;
              result = null;
              isLoading = false;
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        title: Text(
          'Edit KTSP',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dropdownInput1({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive1 == false
                    ? wir1Color
                    : value1Item == ktspModel.tAHUNAJARAN
                        ? wir1Color
                        : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value1Item = null;
                    isActive1 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive1 == false
                        ? wir1Color
                        : value1Item == ktspModel.tAHUNAJARAN
                            ? wir1Color
                            : m1Color,
                  ),
                  value: value1Item == null
                      ? value1Item = ktspModel.tAHUNAJARAN
                      : value1Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listTahunAjaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['tahun_ajaran'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value1Item != value['ID']
                                  ? wir1Color
                                  : value1Item == ktspModel.tAHUNAJARAN
                                      ? wir1Color
                                      : m1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive1 = true;
                      value1Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonFile() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'File',
            style: labelTextStyle.copyWith(
              color: wir1Color,
            ),
          ),
          TextButton(
            onPressed: () {
              _selectFolder();
            },
            style: TextButton.styleFrom(
              backgroundColor: s4Color,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  'assets/user/icon_upload.png',
                  width: 11,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    (file == null) ? trimmed : file!.name.toString(),
                    style: footerTextStyle.copyWith(
                      color: whiteTextStyleColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget buttonSubmit() {
      return Container(
        height: 46,
        margin: EdgeInsets.only(top: 40, bottom: 24),
        width: double.infinity,
        child: TextButton(
          onPressed: () async {
            handleEdit();
          },
          style: TextButton.styleFrom(
              backgroundColor: m1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              )),
          child: isLoading == false
              ? Text(
                  'Simpan',
                  style: buttonTextStyle.copyWith(
                    fontWeight: bold,
                    color: whiteTextStyleColor,
                  ),
                )
              : Container(
                  width: 14,
                  height: 14,
                  child: CircularProgressIndicator(
                    color: whiteTextStyleColor,
                    strokeWidth: 4,
                  ),
                ),
        ),
      );
    }

    Widget buttonCancel() {
      return Container(
        height: 46,
        margin: EdgeInsets.only(
          bottom: 40,
        ),
        width: double.infinity,
        child: TextButton(
          onPressed: () {
            value1Item = null;
            isActive1 = false;
            file = null;
            result = null;
            isLoading = false;
            Navigator.pop(context);
          },
          style: TextButton.styleFrom(
              backgroundColor: whiteTextStyleColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              ),
              side: BorderSide(
                color: mono3Color,
                width: 2,
              )),
          child: Text(
            'Batal',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
              color: mono3Color,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        padding: EdgeInsets.only(
          top: 15,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            dropdownInput1(
              title: 'Tahun Ajaran',
              hint: 'Tahun Ajaran',
            ),
            buttonFile(),
            buttonSubmit(),
            buttonCancel(),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        value1Item = null;
        isActive1 = false;
        file = null;
        result = null;
        isLoading = false;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

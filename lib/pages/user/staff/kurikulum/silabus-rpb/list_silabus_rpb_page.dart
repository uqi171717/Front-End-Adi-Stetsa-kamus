import 'package:adistetsa/models/silabusrpb_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/services/auth_service.dart';
import 'package:adistetsa/theme.dart';
import 'package:adistetsa/widget/loading.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ListSilabusRpbPage extends StatefulWidget {
  @override
  _ListSilabusRpbPageState createState() => _ListSilabusRpbPageState();
}

class _ListSilabusRpbPageState extends State<ListSilabusRpbPage> {
  Object? valueItem1;
  Object? valueItem2;
  Object? valueItem3;
  Object? valueItem4;
  int flag1 = 0;
  int flag2 = 0;
  int flag3 = 0;
  int flag4 = 0;
  var url = '';
  String urlMataPelajaran = '';
  String urlTahunAjaran = '';
  String urlKelas = '';
  String urlSemester = '';
  bool isLoading = false;
  String message = 'Tidak Ada';
  bool isSearch = false;
  TextEditingController searchController = TextEditingController();
  String urlSearch = '';

  @override
  Widget build(BuildContext context) {
    setState(() {});
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    url = '$urlSearch&$urlMataPelajaran&$urlTahunAjaran&$urlKelas&$urlSemester';

    launchUrl({required String url}) async {
      print(url);
      launch(
        url,
      );
    }

    PreferredSizeWidget searchAppbar() {
      return AppBar(
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: true,
        leading: GestureDetector(
          onTap: () async {
            setState(() {
              searchController.clear();
              urlSearch = '';
              isSearch = false;
              isLoading = true;
            });
            await authProvider.getSilabus(url);
            setState(() {
              isLoading = false;
            });
          },
          child: Icon(
            Icons.arrow_back,
            color: wir1Color,
          ),
        ),
        title: TextFormField(
          controller: searchController,
          decoration: InputDecoration(
            hintText: 'Search',
            isDense: true,
            border: InputBorder.none,
          ),
          onChanged: (newValue) async {
            setState(() {
              if (searchController.selection.start >
                  searchController.text.length) {
                searchController.selection = new TextSelection.fromPosition(
                    new TextPosition(offset: searchController.text.length));
                searchController.text = newValue.toString();
              }
              print(searchController.text);
              urlSearch = 'search=${searchController.text}';
              isLoading = true;
            });
            await authProvider.getSilabus(url);
            setState(() {
              isLoading = false;
            });
          },
        ),
        elevation: 4,
        centerTitle: false,
      );
    }

    PreferredSizeWidget silabusAppbar() {
      return AppBar(
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        title: Text(
          'Silabus RPB',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: FontWeight.w600,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pushNamedAndRemoveUntil(
                context, '/main-page/back', (route) => false);
          },
          child: Icon(
            Icons.arrow_back,
            color: wir1Color,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  isSearch = true;
                });
              },
              child: Icon(
                Icons.search,
                color: wir1Color,
              ),
            ),
          ),
        ],
        elevation: 4,
        centerTitle: true,
      );
    }

    downloadModal(String? fileURL) async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: whiteTextStyleColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 20,
              ),
              width: 305,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Text(
                          'Notifikasi',
                          style: buttonTextStyle.copyWith(
                            color: wir1Color,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/cancel_button.png',
                            width: 14,
                            height: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Klik tombol di bawah untuk mengunduh file',
                    style: subtitleProfileTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  TextButton(
                    onPressed: () {
                      launchUrl(url: fileURL.toString());
                    },
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 6,
                      ),
                      backgroundColor: m1Color,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/user/icon_download.png',
                          width: 11,
                          height: 11,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Download',
                          style: labelTextStyle.copyWith(
                            color: whiteTextStyleColor,
                            fontWeight: semiBold,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    deleteModal(int? id) async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: whiteTextStyleColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 20,
              ),
              width: 305,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Text(
                          'Hapus File',
                          style: buttonTextStyle.copyWith(
                            color: wir1Color,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/cancel_button.png',
                            width: 14,
                            height: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Klik tombol di bawah untuk menghapus file',
                    style: subtitleProfileTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: mono2Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Tidak',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      TextButton(
                        onPressed: () async {
                          setState(() {
                            isLoading = true;
                          });
                          Navigator.pop(context);
                          await authProvider.deleteSilabus(id: id);

                          setState(() {
                            isLoading = false;
                          });
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: d1Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Hapus',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    Widget expandList({
      required String header,
      required String subtitle,
      required List content,
      required int? id,
      required String fileURL,
    }) {
      return ExpandableNotifier(
          child: Padding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          children: [
            ExpandablePanel(
              theme: const ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                tapBodyToCollapse: true,
              ),
              header: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    header,
                    style: heading2TextStyle.copyWith(
                      color: wir1Color,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Text(
                    subtitle,
                    style: labelTextStyle,
                  ),
                ],
              ),
              collapsed: Container(),
              expanded: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: content.map((item) {
                      return Text(
                        item.toString(),
                        style: labelTextStyle,
                        softWrap: true,
                        overflow: TextOverflow.fade,
                      );
                    }).toList(),
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    children: [
                      TextButton(
                        onPressed: () {
                          downloadModal(fileURL);
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: s4Color,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/user/icon_download.png',
                              width: 11,
                              height: 11,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Download',
                              style: labelTextStyle.copyWith(
                                color: whiteTextStyleColor,
                                fontWeight: semiBold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      OutlinedButton(
                        onPressed: () async {
                          setState(() {
                            loading(context);
                          });
                          if (await authProvider.getSilabusRPBID(id: id)) {
                            Navigator.pushReplacementNamed(
                                context, '/staff/silabus-rpb/edit/');
                          } else {
                            print("GAGAL MENGAMBIL DATA");
                          }
                        },
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                            color: mono2Color,
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 6,
                          ),
                          backgroundColor: Colors.transparent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/user/icon_edit.png',
                              width: 11,
                              height: 11,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Edit',
                              style: labelTextStyle.copyWith(
                                color: mono2Color,
                                fontWeight: semiBold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: 38,
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              deleteModal(id);
                            });
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: d1Color,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                          child: Image.asset(
                            'assets/user/icon_delete.png',
                            width: 14,
                            height: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 0.5,
            ),
          ],
        ),
      ));
    }

    Widget dropdownList1({required String hint}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag1 == 1 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () async {
                setState(() {
                  isLoading = true;
                  flag1 = 0;
                  valueItem1 = null;
                  urlMataPelajaran = '';
                });
                await authProvider.getSilabus(url);
                setState(() {
                  isLoading = false;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag1 == 1 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag1 == 1 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: valueItem1,
                items: authProvider.listMataPelajaran.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['KODE'],
                      child: Text(
                        value['NAMA'],
                        style: labelTextStyle.copyWith(
                          color:
                              valueItem1 == value['KODE'] ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) async {
                  setState(() {
                    isLoading = true;
                    print(value);
                    valueItem1 = value;
                    flag1 = 1;
                    urlMataPelajaran = 'MATA_PELAJARAN=$valueItem1';
                  });
                  await authProvider.getSilabus(url);

                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ),
          ));
    }

// NOTE: BELUM FIX
    Widget dropdownList2({required String hint}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag2 == 2 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () async {
                setState(() {
                  isLoading = true;
                  urlTahunAjaran = '';
                  flag2 = 0;
                  valueItem2 = null;
                });
                await authProvider.getSilabus(url);
                setState(() {
                  isLoading = false;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag2 == 2 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag2 == 2 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: valueItem2,
                items: authProvider.listTahunAjaran.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['ID'],
                      child: Text(
                        value['tahun_ajaran'],
                        style: labelTextStyle.copyWith(
                          color:
                              valueItem2 == value['ID'] ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) async {
                  setState(() {
                    isLoading = true;
                    flag2 = 2;
                    print(value);
                    valueItem2 = value;
                    urlTahunAjaran = 'TAHUN_AJARAN=$valueItem2';
                  });
                  await authProvider.getSilabus(url);
                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ),
          ));
    }

    Widget dropdownList3({required String hint}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag3 == 3 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () async {
                setState(() {
                  isLoading = true;
                  urlKelas = '';
                  flag3 = 0;
                  valueItem3 = null;
                });
                await authProvider.getSilabus(url);
                setState(() {
                  isLoading = false;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag3 == 3 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag3 == 3 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: valueItem3,
                items: authProvider.listKelas.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['ID'],
                      child: Text(
                        value['KODE_KELAS'],
                        style: labelTextStyle.copyWith(
                          color:
                              valueItem3 == value['ID'] ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) async {
                  setState(() {
                    isLoading = true;
                    flag3 = 3;
                    print(value);
                    valueItem3 = value;
                    urlKelas = 'KELAS=$valueItem3';
                  });
                  await authProvider.getSilabus(url);
                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ),
          ));
    }

    Widget dropdownList4({required String hint}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag4 == 4 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () async {
                setState(() {
                  isLoading = true;
                  urlSemester = '';
                  flag4 = 0;
                  valueItem4 = null;
                });
                await authProvider.getSilabus(url);
                setState(() {
                  isLoading = false;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag4 == 4 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag4 == 4 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: valueItem4,
                items: authProvider.listSemester.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['id'],
                      child: Text(
                        value['NAMA'],
                        style: labelTextStyle.copyWith(
                          color:
                              valueItem4 == value['id'] ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) async {
                  setState(() {
                    isLoading = true;
                    flag4 = 4;
                    print(value);
                    valueItem4 = value;
                    urlSemester = 'SEMESTER=$valueItem4';
                  });
                  await authProvider.getSilabus(url);
                  setState(() {
                    isLoading = false;
                  });
                },
              ),
            ),
          ));
    }

    Widget filter() {
      return Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    // NOTE: ini adalah button reset yang hanya akan muncul jika salah satu dropdown dipilih
                    valueItem1 != null ||
                            valueItem2 != null ||
                            valueItem3 != null ||
                            valueItem4 != null
                        ? GestureDetector(
                            onTap: () async {
                              setState(() {
                                isLoading = true;
                                url = '';
                                urlMataPelajaran = '';
                                urlTahunAjaran = '';
                                urlKelas = '';
                                urlSemester = '';
                                valueItem1 = null;
                                valueItem2 = null;
                                valueItem3 = null;
                                valueItem4 = null;
                                flag1 = 0;
                                flag2 = 0;
                                flag3 = 0;
                                flag4 = 0;
                              });

                              await authProvider.getSilabus(url);
                              setState(() {
                                isLoading = false;
                              });
                            },
                            child: Container(
                              height: 24,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: s4Color,
                              ),
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/user/reset_icon.png',
                                    height: 8,
                                    width: 8,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    'reset',
                                    style: labelTextStyle.copyWith(
                                      color: whiteTextStyleColor,
                                      fontWeight: semiBold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),

                    dropdownList1(
                      hint: 'Mata Pelajaran',
                    ),
                    dropdownList2(
                      hint: 'Tahun Ajaran',
                    ),
                    dropdownList3(
                      hint: 'Kelas',
                    ),
                    dropdownList4(
                      hint: 'Semester',
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushNamedAndRemoveUntil(
            context, '/main-page/back', (route) => false);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, '/staff/silabus-rpb/tambah/');
          },
          tooltip: 'Tambah Silabus RPB',
          child: const Icon(
            Icons.add,
            size: 27.44,
          ),
          backgroundColor: s3Color,
        ),
        appBar: isSearch == true ? searchAppbar() : silabusAppbar(),
        body: Column(
          children: [
            filter(),
            Expanded(
              child: isLoading != true
                  ? ListView(
                      children: [
                        FutureBuilder(
                          future: AuthService().getSilabusRPB(url),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              List<SilabusRPBModel> data = snapshot.data;

                              return data.isEmpty
                                  ? Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.81,
                                      child: Center(
                                        child: Text(
                                          'Data tidak ditemukan',
                                          style: labelTextStyle.copyWith(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                    )
                                  : Column(
                                      children: data.map((item) {
                                      return expandList(
                                        id: item.iD,
                                        fileURL: item.nAMAFILE,
                                        header: item.mATAPELAJARAN.toString(),
                                        subtitle: '${item.kELAS}',
                                        content: ['${item.sEMESTER}'],
                                      );
                                    }).toList());
                            } else {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.81,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: Colors.white,
                                        backgroundColor: s1Color,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        )
                      ],
                    )
                  : Container(),
            ),
          ],
        ),
      ),
    );
  }
}

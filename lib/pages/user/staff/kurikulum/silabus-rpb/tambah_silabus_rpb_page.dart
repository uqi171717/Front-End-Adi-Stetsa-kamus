import 'package:adistetsa/providers/auth_providers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class TambahSilabusRPBPage extends StatefulWidget {
  const TambahSilabusRPBPage({Key? key}) : super(key: key);

  @override
  _TambahSilabusRPBPageState createState() => _TambahSilabusRPBPageState();
}

Object? value1Item;
Object? value2Item;
Object? value3Item;
Object? value4Item;

bool isActive1 = false;
bool isActive2 = false;
bool isActive3 = false;
bool isActive4 = false;
bool isLoading = false;
PlatformFile? file;
FilePickerResult? result;

class _TambahSilabusRPBPageState extends State<TambahSilabusRPBPage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    _selectFolder() async {
      result = await FilePicker.platform.pickFiles(
          type: FileType.custom, allowedExtensions: ['pdf', 'doc', 'docx']);
      if (result != null) {
        setState(() {
          file = result!.files.first;
        });
      }
    }

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 4,
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
            onTap: () {
              value1Item = null;
              value2Item = null;
              value3Item = null;
              value4Item = null;
              isActive1 = false;
              isActive2 = false;
              isActive3 = false;
              isActive4 = false;
              file = null;
              result = null;
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        title: Text(
          'Tambah Silabus RPB',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dropdownInput1({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive1 == false ? mono3Color : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value1Item = null;
                    isActive1 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive1 == false ? mono3Color : m1Color,
                  ),
                  value: value1Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listMataPelajaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['NAMA'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value1Item == value['KODE']
                                  ? m1Color
                                  : wir1Color,
                            ),
                          ),
                          value: value['KODE'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive1 = true;
                      value1Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput2({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive2 == false ? mono3Color : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value2Item = null;
                    isActive2 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive2 == false ? mono3Color : m1Color,
                  ),
                  value: value2Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listTahunAjaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['tahun_ajaran'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value2Item == value['ID']
                                  ? m1Color
                                  : wir1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive2 = true;
                      value2Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput3({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive3 == false ? mono3Color : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value3Item = null;
                    isActive3 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive3 == false ? mono3Color : m1Color,
                  ),
                  value: value3Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listKelas
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['KODE_KELAS'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value3Item == value['ID']
                                  ? m1Color
                                  : wir1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive3 = true;
                      value3Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput4({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive4 == false ? mono3Color : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value4Item = null;
                    isActive4 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive4 == false ? mono3Color : m1Color,
                  ),
                  value: value4Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listSemester
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['NAMA'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value4Item == value['id']
                                  ? m1Color
                                  : wir1Color,
                            ),
                          ),
                          value: value['id'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive4 = true;
                      value4Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonFile() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'File',
            style: labelTextStyle.copyWith(
              color: wir1Color,
            ),
          ),
          TextButton(
            onPressed: () {
              _selectFolder();
            },
            style: TextButton.styleFrom(
              backgroundColor: s4Color,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/user/icon_upload.png',
                  width: 11,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    (file == null) ? 'Pilih File' : file!.name.toString(),
                    style: footerTextStyle.copyWith(
                      color: whiteTextStyleColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget buttonSubmit() {
      return Container(
        height: 46,
        margin: EdgeInsets.symmetric(
          vertical: 40,
        ),
        width: double.infinity,
        child: TextButton(
          onPressed: () async {
            if (isActive1 &&
                isActive2 &&
                isActive3 &&
                isActive4 &&
                file != null) {
              if (file!.extension == 'DOC' ||
                  file!.extension == 'DOCX' ||
                  file!.extension == 'pdf') {
                setState(() {
                  isLoading = true;
                });
                await authProvider.tambahSilabusRPB(
                    value1Item.toString(),
                    int.parse(value2Item.toString()),
                    int.parse(value3Item.toString()),
                    int.parse(value4Item.toString()),
                    file!.path);

                setState(() {
                  value1Item = null;
                  value2Item = null;
                  value3Item = null;
                  value4Item = null;
                  isActive1 = false;
                  isActive2 = false;
                  isActive3 = false;
                  isActive4 = false;
                  file = null;
                  result = null;
                  isLoading = false;
                });

                Navigator.pushNamed(context, '/staff/silabus-rpb/kembali');
              } else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  backgroundColor: d1Color,
                  content: Text(
                    "Format file tidak didukung",
                    textAlign: TextAlign.center,
                  ),
                ));
              }
            } else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: d1Color,
                content: Text(
                  "Anda belum mengisi semua data yang dibutuhkan",
                  textAlign: TextAlign.center,
                ),
              ));
            }
          },
          style: TextButton.styleFrom(
              backgroundColor: m1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              )),
          child: isLoading == false
              ? Text(
                  'Simpan',
                  style: buttonTextStyle.copyWith(
                    fontWeight: bold,
                    color: whiteTextStyleColor,
                  ),
                )
              : Container(
                  width: 14,
                  height: 14,
                  child: CircularProgressIndicator(
                    color: whiteTextStyleColor,
                    strokeWidth: 4,
                  ),
                ),
        ),
      );
    }

    Widget content() {
      return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          padding: EdgeInsets.only(
            top: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              dropdownInput1(
                title: 'Materi Pelajaran',
                hint: 'Materi Pelajaran',
              ),
              dropdownInput2(
                title: 'Tahun Ajaran',
                hint: 'Tahun Ajaran',
              ),
              dropdownInput3(
                title: 'Kelas',
                hint: 'Kelas',
              ),
              dropdownInput4(
                title: 'Semester',
                hint: 'Materi Pelajaran',
              ),
              buttonFile(),
              buttonSubmit(),
            ],
          ),
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        value1Item = null;
        value2Item = null;
        value3Item = null;
        value4Item = null;
        isActive1 = false;
        isActive2 = false;
        isActive3 = false;
        isActive4 = false;
        file = null;
        result = null;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

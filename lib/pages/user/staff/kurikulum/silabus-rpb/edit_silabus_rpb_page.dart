import 'package:adistetsa/models/silabusrpb_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class EditSilabusRPBPage extends StatefulWidget {
  EditSilabusRPBPage({Key? key}) : super(key: key);

  @override
  _EditSilabusRPBPageState createState() => _EditSilabusRPBPageState();
}

Object? value1Item;
Object? value2Item;
Object? value3Item;
Object? value4Item;
var value5Item;

bool isActive1 = false;
bool isActive2 = false;
bool isActive3 = false;
bool isActive4 = false;
PlatformFile? file;
FilePickerResult? result;
bool isLoading = false;
int? filesize;

class _EditSilabusRPBPageState extends State<EditSilabusRPBPage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    SilabusRPBModel silabusRPBModel = authProvider.SilabusRPB;

    String link = silabusRPBModel.nAMAFILE.toString();

    String delimiter = 'RPB/';
    int lastIndex = link.indexOf(delimiter);

    String trimmed = link.substring(lastIndex + 4, link.length);

    _selectFolder() async {
      result = await FilePicker.platform.pickFiles(
          type: FileType.custom, allowedExtensions: ['pdf', 'doc', 'docx']);

      if (result != null) {
        setState(() {
          file = result!.files.first;
          filesize = file?.size;
        });
      }
    }

    handleSubmit(int? fileSize) async {
      setState(() {
        isLoading = true;
      });

      if (await authProvider.editSilabusRPB(
          silabusRPBModel.iD,
          value1Item.toString(),
          int.parse(value2Item.toString()),
          int.parse(value3Item.toString()),
          int.parse(value4Item.toString()),
          file == null ? null : file!.path)) {
        if (file != null) {
          if (file!.extension == 'DOC' ||
              file!.extension == 'DOCX' ||
              file!.extension == 'pdf') {
            value1Item = null;
            value2Item = null;
            value3Item = null;
            value4Item = null;
            file = null;
            result = null;
            isActive1 = false;
            isActive2 = false;
            isActive3 = false;
            isActive4 = false;
            isLoading = false;
            return Navigator.pushNamed(context, '/staff/silabus-rpb/kembali');
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: d1Color,
              content: Text(
                "Format File tidak didukung",
                textAlign: TextAlign.center,
              ),
            ));
          }
        } else {
          value1Item = null;
          value2Item = null;
          value3Item = null;
          value4Item = null;
          file = null;
          result = null;
          isActive1 = false;
          isActive2 = false;
          isActive3 = false;
          isActive4 = false;
          isLoading = false;
          return Navigator.pushNamed(context, '/staff/silabus-rpb/kembali');
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: d1Color,
          content: Text(
            "Gagal Melakukan Update",
            textAlign: TextAlign.center,
          ),
        ));
      }
      setState(() {
        isLoading = false;
      });
    }

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 2,
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
            onTap: () {
              value1Item = null;
              value2Item = null;
              value3Item = null;
              value4Item = null;
              file = null;
              result = null;
              isActive1 = false;
              isActive2 = false;
              isActive3 = false;
              isActive4 = false;

              isLoading = false;
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_sharp)),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        title: Text(
          'Edit Silabus RPB',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dropdownInput1({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive1 == false
                    ? wir1Color
                    : value1Item == silabusRPBModel.mATAPELAJARAN
                        ? wir1Color
                        : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value1Item = null;
                    isActive1 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive1 == false
                        ? wir1Color
                        : value1Item == silabusRPBModel.mATAPELAJARAN
                            ? wir1Color
                            : m1Color,
                  ),
                  value: value1Item == null
                      ? value1Item = silabusRPBModel.mATAPELAJARAN
                      : value1Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listMataPelajaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['NAMA'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value1Item == silabusRPBModel.mATAPELAJARAN
                                  ? wir1Color
                                  : value1Item != value['KODE']
                                      ? wir1Color
                                      : m1Color,
                            ),
                          ),
                          value: value['KODE'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive1 = true;
                      value1Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput2({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive2 == false
                    ? wir1Color
                    : value2Item == silabusRPBModel.tAHUNAJARAN
                        ? wir1Color
                        : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value2Item = null;
                    isActive2 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive2 == false
                        ? wir1Color
                        : value2Item == silabusRPBModel.tAHUNAJARAN
                            ? wir1Color
                            : m1Color,
                  ),
                  value: value2Item == null
                      ? value2Item = silabusRPBModel.tAHUNAJARAN
                      : value2Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listTahunAjaran
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['tahun_ajaran'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value['ID'] != value2Item
                                  ? wir1Color
                                  : value2Item == silabusRPBModel.tAHUNAJARAN
                                      ? wir1Color
                                      : m1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive2 = true;
                      value2Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput3({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive3 == false
                    ? wir1Color
                    : value3Item == silabusRPBModel.kELAS
                        ? wir1Color
                        : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value3Item = null;
                    isActive3 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive3 == false
                        ? wir1Color
                        : value3Item == silabusRPBModel.kELAS
                            ? wir1Color
                            : m1Color,
                  ),
                  value: value3Item == null
                      ? value3Item = silabusRPBModel.kELAS
                      : value3Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listKelas
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['KODE_KELAS'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value['ID'] != value3Item
                                  ? wir1Color
                                  : value3Item == silabusRPBModel.kELAS
                                      ? wir1Color
                                      : m1Color,
                            ),
                          ),
                          value: value['ID'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive3 = true;
                      value3Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget dropdownInput4({required String title, required String hint}) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: labelTextStyle.copyWith(
              color: wir1Color,
              fontWeight: regular,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                8,
              ),
              border: Border.all(
                color: isActive4 == false
                    ? wir1Color
                    : value4Item == silabusRPBModel.sEMESTER
                        ? wir1Color
                        : m1Color,
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    value4Item = null;
                    isActive4 = false;
                  });
                },
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text(
                    hint,
                    style: subtitleFooterTextStyle,
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_outlined,
                    color: isActive4 == false
                        ? m1Color
                        : value4Item == silabusRPBModel.sEMESTER
                            ? wir1Color
                            : m1Color,
                  ),
                  value: value4Item == null
                      ? value4Item = silabusRPBModel.sEMESTER
                      : value4Item,
                  dropdownColor: whiteTextStyleColor,
                  items: authProvider.listSemester
                      .map(
                        (value) => DropdownMenuItem(
                          child: Text(
                            value['NAMA'],
                            style: subtitleFooterTextStyle.copyWith(
                              color: value['id'] != value4Item
                                  ? wir1Color
                                  : value4Item == silabusRPBModel.sEMESTER
                                      ? wir1Color
                                      : m1Color,
                            ),
                          ),
                          value: value['id'],
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      isActive4 = true;
                      value4Item = value;
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonFile() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'File',
            style: labelTextStyle.copyWith(
              color: wir1Color,
            ),
          ),
          TextButton(
            onPressed: () {
              _selectFolder();
            },
            style: TextButton.styleFrom(
              backgroundColor: s4Color,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/user/icon_upload.png',
                  width: 11,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    (file == null) ? trimmed : file!.name.toString(),
                    style: footerTextStyle.copyWith(
                      color: whiteTextStyleColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }

    Widget buttonLoadingSubmit() {
      return Container(
        height: 46,
        margin: EdgeInsets.only(top: 40, bottom: 24),
        width: double.infinity,
        child: TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
              backgroundColor: m1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              )),
          child: Container(
            width: 18,
            height: 18,
            child: CircularProgressIndicator(
              color: whiteTextStyleColor,
              strokeWidth: 4,
            ),
          ),
        ),
      );
    }

    Widget buttonSubmit() {
      return Container(
        height: 46,
        margin: EdgeInsets.only(top: 40, bottom: 24),
        width: double.infinity,
        child: TextButton(
          onPressed: () {
            handleSubmit(filesize);
          },
          style: TextButton.styleFrom(
              backgroundColor: m1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              )),
          child: Text(
            'Simpan',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
              color: whiteTextStyleColor,
            ),
          ),
        ),
      );
    }

    Widget buttonCancel() {
      return Container(
        height: 46,
        margin: EdgeInsets.only(
          bottom: 40,
        ),
        width: double.infinity,
        child: TextButton(
          onPressed: () {
            value1Item = null;
            value2Item = null;
            value3Item = null;
            value4Item = null;
            file = null;
            result = null;
            isActive1 = false;
            isActive2 = false;
            isActive3 = false;
            isActive4 = false;
            isLoading = false;

            Navigator.pop(context);
          },
          style: TextButton.styleFrom(
              backgroundColor: whiteTextStyleColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              ),
              side: BorderSide(
                color: mono3Color,
                width: 1,
              )),
          child: Text(
            'Batal',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
              color: mono3Color,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        padding: EdgeInsets.only(
          top: 15,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            dropdownInput1(
              title: 'Materi Pelajaran',
              hint: 'Materi Pelajaran',
            ),
            dropdownInput2(
              title: 'Tahun Ajaran',
              hint: 'Tahun Ajaran',
            ),
            dropdownInput3(
              title: 'Kelas',
              hint: 'Kelas',
            ),
            dropdownInput4(
              title: 'Semester',
              hint: 'Semester',
            ),
            buttonFile(),
            isLoading == false ? buttonSubmit() : buttonLoadingSubmit(),
            buttonCancel(),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        value1Item = null;
        value2Item = null;
        value3Item = null;
        value4Item = null;
        value5Item = null;
        file = null;
        result = null;
        isActive1 = false;
        isActive2 = false;
        isActive3 = false;
        isActive4 = false;
        isLoading = false;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

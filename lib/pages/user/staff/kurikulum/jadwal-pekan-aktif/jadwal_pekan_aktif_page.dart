import 'package:adistetsa/models/pekanaktif_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class JadwalPekanAktifPage extends StatefulWidget {
  JadwalPekanAktifPage({Key? key}) : super(key: key);

  @override
  _JadwalPekanAktifPageState createState() => _JadwalPekanAktifPageState();
}

class _JadwalPekanAktifPageState extends State<JadwalPekanAktifPage> {
  Object? value1Item;
  Object? value2Item;
  Object? value3Item;

  int flag1 = 0;
  int flag2 = 0;
  int flag3 = 0;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    PreferredSizeWidget header() {
      return AppBar(
        title: Text(
          'Pekan Aktif',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: wir1Color,
          ),
        ),
        elevation: 4,
        centerTitle: true,
      );
    }

    Widget dropdownList1({required String hint, required List item}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag1 == 1 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () {
                setState(() {
                  flag1 = 0;
                  value1Item = null;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag1 == 1 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag1 == 1 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: value1Item,
                items: authProvider.listMataPelajaran.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['KODE'],
                      child: Text(
                        value['NAMA'],
                        style: labelTextStyle.copyWith(
                          color: value1Item == value ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) {
                  setState(() {
                    print(value);
                    value1Item = value;
                    flag1 = 1;
                  });
                },
              ),
            ),
          ));
    }

    Widget dropdownList2({required String hint, required List item}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag2 == 2 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () {
                setState(() {
                  flag2 = 0;
                  value2Item = null;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag2 == 2 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag2 == 2 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: value2Item,
                items: authProvider.listKelas.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['ID'],
                      child: Text(
                        value['KODE_KELAS'],
                        style: labelTextStyle.copyWith(
                          color: value2Item == value ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) {
                  setState(() {
                    flag2 = 2;
                    print(value);
                    value2Item = value;
                  });
                },
              ),
            ),
          ));
    }

    Widget dropdownList3({required String hint, required List item}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag3 == 3 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () {
                setState(() {
                  flag3 = 0;
                  value3Item = null;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag3 == 3 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag3 == 3 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: value3Item,
                items: authProvider.listSemester.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value['id'],
                      child: Text(
                        value['NAMA'],
                        style: labelTextStyle.copyWith(
                          color: value3Item == value ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) {
                  setState(() {
                    flag3 = 3;
                    print(value);
                    value3Item = value;
                  });
                },
              ),
            ),
          ));
    }

    Widget listItem(
        {int? id, String? semester, String? mataPelajaran, String? kelas}) {
      return GestureDetector(
        onTap: () {
          authProvider.pekanaktif = PekanAktifModel(
              iD: id,
              mATAPELAJARAN: mataPelajaran,
              kELAS: kelas,
              sEMESTER: semester);
          authProvider.getPekanAktif();
          Navigator.pushNamed(
              context, '/staff/pekan-aktif/detail-pekan-aktif/');
        },
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: mono3Color,
                width: 0.5,
              ),
            ),
          ),
          margin: EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: 12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$semester',
                          style: labelTextStyle,
                        ),
                        Text(
                          '$mataPelajaran',
                          style: subtitleProfileTextStyle,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 12,
                    ),
                    child: Row(
                      children: [
                        Text(
                          '$kelas',
                          style: labelTextStyle,
                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Image.asset(
                          'assets/user/icon_detail_pekan_aktif.png',
                          width: 12.32,
                          color: iconColor,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget filter() {
      return Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    // NOTE: ini adalah button reset yang hanya akan muncul jika salah satu dropdown dipilih
                    flag1 != 0 || flag2 != 0 || flag3 != 0
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                value1Item = null;
                                value2Item = null;
                                value3Item = null;

                                flag1 = 0;
                                flag2 = 0;
                                flag3 = 0;
                              });
                            },
                            child: Container(
                              height: 24,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: s4Color,
                              ),
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/user/reset_icon.png',
                                    height: 8,
                                    width: 8,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    'reset',
                                    style: labelTextStyle.copyWith(
                                      color: whiteTextStyleColor,
                                      fontWeight: semiBold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),
                    dropdownList1(
                      hint: 'Mata Pelajaran',
                      item: [
                        'Matematika',
                        'Bahasa Indonesia',
                        'Kimia',
                        'Biologi',
                      ],
                    ),

                    dropdownList2(
                      hint: 'Kelas',
                      item: [
                        'x',
                        'XI',
                        'XII',
                      ],
                    ),
                    dropdownList3(
                      hint: 'Semester',
                      item: [
                        'Ganjil',
                        'Genap',
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: Column(
        children: [
          filter(),
          Expanded(
            child: ListView(
              children: [
                FutureBuilder(
                  future: AuthService().getPekanAktif(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      List<PekanAktifModel> data = snapshot.data;
                      return Column(
                          children: data.map((item) {
                        return listItem(
                          id: item.iD,
                          semester: item.sEMESTER.toString(),
                          mataPelajaran: item.mATAPELAJARAN,
                          kelas: item.kELAS.toString(),
                        );
                      }).toList());
                    } else {
                      return Container(
                        height: MediaQuery.of(context).size.height * 0.70,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 25,
                              height: 25,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                color: Colors.white,
                                backgroundColor: s1Color,
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

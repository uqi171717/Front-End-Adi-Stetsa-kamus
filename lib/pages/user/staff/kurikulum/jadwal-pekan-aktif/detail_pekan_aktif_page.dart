import 'package:adistetsa/models/mingguefektif_model.dart';
import 'package:adistetsa/models/minggutidakefektif_model.dart';
import 'package:adistetsa/models/pekanaktif_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class DetailPekanAktifPage extends StatefulWidget {
  DetailPekanAktifPage({Key? key}) : super(key: key);

  @override
  _DetailPekanAktifPageState createState() => _DetailPekanAktifPageState();
}

class _DetailPekanAktifPageState extends State<DetailPekanAktifPage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    PekanAktifModel pekanAktifModel = authProvider.PekanAktif;
    print(pekanAktifModel.iD);

    PreferredSizeWidget header() {
      return AppBar(
        title: Text(
          'Detail Pekan Aktif',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: whiteTextStyleColor,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: wir1Color,
          ),
        ),
        elevation: 4,
        centerTitle: true,
        bottom: TabBar(
          indicatorColor: m1Color,
          labelColor: wir1Color,
          unselectedLabelColor: mono3Color,
          labelStyle: labelTextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
          tabs: [
            Tab(
              text: 'Pekan Efektif',
            ),
            Tab(
              text: 'Pekan Tidak Efektif',
            ),
          ],
        ),
      );
    }

    Widget itemCardPekanAktif(
        {String? bulan,
        int? jumlahMinggu,
        int? mingguTidakEfektif,
        int? mingguAktif,
        String? keterangan}) {
      // print(mingguEfektif);
      return Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: mono3Color,
              width: 0.5,
            ),
          ),
        ),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Text(
              '$bulan',
              style: buttonTextStyle.copyWith(
                fontWeight: semiBold,
                color: wir1Color,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Jumlah Minggu',
                      style: labelTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '$jumlahMinggu',
                      style: buttonTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Minggu Tidak Efektif',
                      style: labelTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '$mingguTidakEfektif',
                      style: buttonTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Minggu Aktif',
                      style: labelTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '$mingguAktif',
                      style: buttonTextStyle.copyWith(
                        color: wir1Color,
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  'Keterangan : ',
                  style: labelTextStyle.copyWith(
                    color: wir1Color,
                    fontWeight: bold,
                  ),
                ),
                Text(
                  '$keterangan',
                  style: labelTextStyle.copyWith(
                    color: wir1Color,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 17,
            ),
          ],
        ),
      );
    }

    Widget itemCardPekanTidakEfektif(
        {String? uraiankegiatan, int? jumlahMinggu, String? keterangan}) {
      return Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: mono3Color,
              width: 0.5,
            ),
          ),
        ),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Text(
              '$uraiankegiatan',
              style: buttonTextStyle.copyWith(
                fontWeight: semiBold,
                color: wir1Color,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Jumlah Minggu',
              style: labelTextStyle.copyWith(
                color: wir1Color,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              '$jumlahMinggu',
              style: buttonTextStyle.copyWith(
                color: wir1Color,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Keterangan : ',
                  style: labelTextStyle.copyWith(
                    color: wir1Color,
                    fontWeight: bold,
                  ),
                ),
                Expanded(
                  child: Text(
                    '$keterangan',
                    style: labelTextStyle.copyWith(
                      color: wir1Color,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 17,
            ),
          ],
        ),
      );
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: whiteTextStyleColor,
          appBar: header(),
          body: TabBarView(
            children: [
              Column(
                children: [
                  Container(
                    height: 80,
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 34,
                    ),
                    decoration: BoxDecoration(
                      color: s1Color,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                         '${pekanAktifModel.mATAPELAJARAN}',
                          style: buttonTextStyle.copyWith(
                            fontWeight: semiBold,
                          ),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Text(
                          '${pekanAktifModel.kELAS}',
                          style: labelTextStyle.copyWith(
                            fontWeight: semiBold,
                            color: whiteTextStyleColor,
                          ),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Text(
                          '${pekanAktifModel.sEMESTER}',
                          style: labelTextStyle.copyWith(
                            fontWeight: semiBold,
                            color: whiteTextStyleColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        FutureBuilder(
                          future: AuthService()
                              .getMingguEfektif(id: pekanAktifModel.iD),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              List<MingguEfektifModel> data = snapshot.data;
                              // print(data);
                              return Column(
                                  children: data.map((item) {
                                return itemCardPekanAktif(
                                  bulan: item.bULAN,
                                  jumlahMinggu: item.jUMLAHMINGGU,
                                  mingguTidakEfektif:
                                      item.jUMLAHMINGGUTIDAKEFEKTIF,
                                  mingguAktif: item.jUMLAHMINGGUEFEKTIF,
                                  keterangan: item.kETERANGAN,
                                );
                              }).toList());
                            } else {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.70,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: Colors.white,
                                        backgroundColor: s1Color,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    height: 80,
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 34,
                    ),
                    decoration: BoxDecoration(
                      color: s1Color,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '${pekanAktifModel.mATAPELAJARAN}',
                            style: buttonTextStyle.copyWith(
                              fontWeight: semiBold,
                            ),
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            '${pekanAktifModel.kELAS}',
                            style: labelTextStyle.copyWith(
                              fontWeight: semiBold,
                              color: whiteTextStyleColor,
                            ),
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            '${pekanAktifModel.sEMESTER}',
                            style: labelTextStyle.copyWith(
                              fontWeight: semiBold,
                              color: whiteTextStyleColor,
                            ),
                          ),
                        ]),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        FutureBuilder(
                          future: AuthService()
                              .getMingguTidakEfektif(id: pekanAktifModel.iD),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              List<MingguTidakEfektifModel> data =
                                  snapshot.data;
                              // print(data);
                              return Column(
                                  children: data.map((item) {
                                return itemCardPekanTidakEfektif(
                                  uraiankegiatan: item.uRAIANKEGIATAN,
                                  jumlahMinggu: item.jUMLAHMINGGU,
                                  keterangan: item.kETERANGAN,
                                );
                              }).toList());
                            } else {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.70,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 25,
                                      height: 25,
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: Colors.white,
                                        backgroundColor: s1Color,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

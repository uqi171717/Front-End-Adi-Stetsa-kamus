import 'package:adistetsa/providers/auth_providers.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class DetailDataSiswaPage extends StatefulWidget {
  DetailDataSiswaPage({Key? key}) : super(key: key);

  @override
  _DetailDataSiswaPageState createState() => _DetailDataSiswaPageState();
}

class _DetailDataSiswaPageState extends State<DetailDataSiswaPage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    var data = authProvider.Siswa;

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 2,
        backgroundColor: whiteTextStyleColor,
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        centerTitle: true,
        title: Text(
          'Detail Siswa',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget detailNama(String? name, String? nis) {
      return Container(
        height: 67,
        width: double.infinity,
        color: m1Color,
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 12,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '$name',
              style: headingTextStyle.copyWith(
                color: whiteTextStyleColor,
                fontWeight: bold,
              ),
            ),
            Text(
              '$nis',
              style: footerTextStyle.copyWith(
                color: whiteTextStyleColor,
              ),
            ),
          ],
        ),
      );
    }

    Widget detailItem(String? name, String? value) {
      return Container(
        margin: EdgeInsets.only(
          bottom: 20,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 40,
        ),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 100,
                child: Text(
                  name.toString(),
                  style: footerTextStyle.copyWith(
                    color: wir1Color,
                    fontWeight: semiBold,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
              Expanded(
                child: Text(
                  value == '' || value == 'null'
                      ? "Tidak ada data"
                      : value.toString(),
                  style: footerTextStyle.copyWith(
                    color:
                        value != '' && value != 'null' ? wir1Color : redColor,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: Column(
        children: [
          detailNama(data.nAMA, data.nIS),
          SizedBox(
            height: 12,
          ),
          Expanded(
            child: ListView(
              children: [
                detailItem('NISN', data.nIS),
                detailItem('NIK', data.nIK),
                detailItem(
                  'Kelas',
                  authProvider.listKelasSiswa
                      .map((e) {
                        if (data.nIS == e['NIS']) {
                          return e['KELAS'];
                        }
                      })
                      .toList()
                      .toString()
                      .replaceAll('[', '')
                      .replaceAll(']', ''),
                ),
                detailItem('Email', data.eMAIL),
                detailItem('No.HP', data.tELEPON),
                detailItem('TTL', "${data.tEMPATLAHIR}, ${data.tANGGALLAHIR}"),
                detailItem('Gender', data.jENISKELAMIN),
                detailItem('Agama', data.aGAMA),
                detailItem('Alamat', data.aLAMAT),
                detailItem('RT', data.rT.toString()),
                detailItem('RW', data.rW.toString()),
                detailItem('Dusun', data.dUSUN),
                detailItem('Desa', data.dUSUN),
                detailItem('Kecamatan', data.kECAMATAN),
                detailItem('Kode Pos', data.kODEPOS.toString()),
                detailItem('Jenis Tinggal', data.jENISTINGGAL),
                detailItem('Penerima KPS', data.pENERIMAKPS),
                detailItem('Penerima KIP', data.pENERIMAKIP),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

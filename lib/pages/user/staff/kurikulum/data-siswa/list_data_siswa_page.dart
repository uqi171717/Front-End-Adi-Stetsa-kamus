import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';

import 'package:adistetsa/widget/loading.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class ListDataSiswaPage extends StatefulWidget {
  ListDataSiswaPage({Key? key}) : super(key: key);

  @override
  _ListDataSiswaPageState createState() => _ListDataSiswaPageState();
}

class _ListDataSiswaPageState extends State<ListDataSiswaPage> {
  Object? value1Item;
  Object? value2Item;

  int flag1 = 0;
  int flag2 = 0;
  String? tempKelas;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    authProvider.getKelasSiswa(context);
    PreferredSizeWidget header() {
      return AppBar(
        elevation: 2,
        backgroundColor: whiteTextStyleColor,
        iconTheme: IconThemeData(
          color: wir1Color,
        ),
        centerTitle: true,
        title: Text(
          'Data Siswa',
          style: heading4TextStyle.copyWith(
            color: wir1Color,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dropdownList1({required String hint, required List item}) {
      return Container(
          height: 24,
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
          margin: EdgeInsets.symmetric(horizontal: 6),
          decoration: BoxDecoration(
            border: Border.all(
              color: flag1 == 1 ? m1Color : mono3Color,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: DropdownButtonHideUnderline(
            child: GestureDetector(
              onLongPress: () {
                setState(() {
                  flag1 = 0;
                  value1Item = null;
                });
              },
              child: DropdownButton(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: flag1 == 1 ? m1Color : wir1Color,
                ),
                hint: Text(
                  hint,
                  style: labelTextStyle.copyWith(
                    color: flag1 == 1 ? m1Color : wir1Color,
                    fontWeight: regular,
                  ),
                ),
                dropdownColor: whiteTextStyleColor,
                elevation: 2,
                value: value1Item,
                items: item.map(
                  (value) {
                    return DropdownMenuItem(
                      value: value,
                      child: Text(
                        value,
                        style: labelTextStyle.copyWith(
                          color: value1Item == value ? m1Color : wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (value) {
                  setState(() {
                    print(value);
                    value1Item = value;
                    flag1 = 1;
                  });
                },
              ),
            ),
          ));
    }

    Widget filter() {
      return Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    // NOTE: ini adalah button reset yang hanya akan muncul jika salah satu dropdown dipilih
                    flag1 != 0 || flag2 != 0
                        ? GestureDetector(
                            onTap: () {
                              setState(() {
                                value1Item = null;
                                value2Item = null;

                                flag1 = 0;
                                flag2 = 0;
                              });
                            },
                            child: Container(
                              height: 24,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: s4Color,
                              ),
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/user/reset_icon.png',
                                    height: 8,
                                    width: 8,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    'reset',
                                    style: labelTextStyle.copyWith(
                                      color: whiteTextStyleColor,
                                      fontWeight: semiBold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),

                    dropdownList1(
                      hint: 'Kelas',
                      item: [
                        'x',
                        'XI',
                        'XII',
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget listItem({
      required String? name,
      required String? nis,
      required String? kelas,
    }) {
      return GestureDetector(
        onTap: () async {
          loading(context);
          if (await authProvider.getDataSiswaNIS(nis: nis)) {
            Navigator.pushReplacementNamed(
                context, '/staff/data-siswa/detail-data-siswa/');
          }
        },
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: mono3Color,
                width: 0.5,
              ),
            ),
          ),
          margin: EdgeInsets.only(
            left: 20,
            right: 20,
            bottom: 12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$name',
                          style: subtitleProfileTextStyle,
                        ),
                        Text(
                          '$nis',
                          style: labelTextStyle.copyWith(
                            color: wir1Color,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 12,
                    ),
                    child: Row(
                      children: [
                        Text(
                          kelas != 'null' ? kelas.toString() : 'Tidak ada data',
                          style: labelTextStyle.copyWith(
                            color: kelas == 'null' ? redColor : wir1Color,
                          ),
                        ),
                        SizedBox(
                          width: 9.3,
                        ),
                        Image.asset(
                          'assets/user/icon_detail_pekan_aktif.png',
                          width: 12.32,
                          color: iconColor,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: Column(
        children: [
          filter(),
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: authProvider.getDataSiswa(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    List<SiswaModel> data = snapshot.data;
                    return Column(
                      children: data.map((item) {
                        // print(authProvider.listKelasSiswa);

                        return listItem(
                            name: item.nAMA,
                            nis: item.nIS,
                            kelas: authProvider.listKelasSiswa
                                .map((e) {
                                  if (item.nIS == e['NIS']) {
                                    return e['KELAS'];
                                  }
                                })
                                .toList()
                                .toString()
                                .replaceAll('[', '')
                                .replaceAll(']', ''));
                      }).toList(),
                    );
                  } else {
                    return Container(
                      height: MediaQuery.of(context).size.height * 0.70,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 25,
                            height: 25,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                              color: Colors.white,
                              backgroundColor: s1Color,
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

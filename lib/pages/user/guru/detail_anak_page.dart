import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';
// import 'package:adistetsa/theme.dart';

class DetailAnakPage extends StatefulWidget {
  @override
  _DetailAnakPageState createState() => _DetailAnakPageState();
}

class _DetailAnakPageState extends State<DetailAnakPage> {
  int headerflag = 0;

  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Data Anak',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
        actions: [
          TextButton(
            onPressed: () {
              setState(() {
                headerflag = 1;
              });
            },
            child: Text(
              'Edit',
              style: buttonTextStyle.copyWith(
                fontWeight: semiBold,
              ),
            ),
          ),
        ],
      );
    }

    PreferredSizeWidget headerEdit() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Data Anak',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget profileDetail(String title, String text) {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  TextFormField(
                    enabled: headerflag == 0 ? false : true,
                    readOnly: headerflag == 0 ? true : false,
                    decoration: InputDecoration(
                      hintText: text,
                      contentPadding: EdgeInsets.only(
                        bottom: 8,
                      ),
                      hintStyle: subtitleProfileTextStyle,
                      isCollapsed: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: m1Color,
                          width: 0.5,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget buttonDelete() {
      return Container(
        margin: EdgeInsets.only(
          bottom: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 48,
        ),
        width: double.infinity,
        height: 46,
        child: TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
              primary: whiteTextStyleColor,
              backgroundColor: d1Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          child: Text(
            'Hapus',
            style: buttonTextStyle.copyWith(
              fontWeight: semiBold,
            ),
          ),
        ),
      );
    }

    Widget buttonUbah() {
      return Container(
        margin: EdgeInsets.only(
          bottom: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 48,
        ),
        width: double.infinity,
        height: 46,
        child: TextButton(
          onPressed: () {},
          style: TextButton.styleFrom(
              primary: whiteTextStyleColor,
              backgroundColor: s4Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          child: Text(
            'Ubah',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
            ),
          ),
        ),
      );
    }

    Widget buttonBatal() {
      return Container(
        margin: EdgeInsets.only(
          bottom: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 48,
        ),
        width: double.infinity,
        height: 46,
        child: TextButton(
          onPressed: () {
            setState(() {
              headerflag = 0;
            });
          },
          style: TextButton.styleFrom(
              side: BorderSide(
                color: mono3Color,
              ),
              backgroundColor: whiteTextStyleColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          child: Text(
            'Batal',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
              color: mono3Color,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          children: [
            profileDetail(
              'Nama Anak',
              'Eka Widyanti',
            ),
            profileDetail(
              'NISN',
              '0021241149',
            ),
            profileDetail(
              'Gender',
              'Perempuan',
            ),
            profileDetail(
              'Tempat Lahir',
              'Bangkok',
            ),
            profileDetail(
              'Tanggal Lahir',
              '2002-02-22',
            ),
            profileDetail(
              'Jenjang',
              'S1',
            ),
            profileDetail(
              'Status',
              'Kandung',
            ),
            profileDetail(
              'Tahun Masuk',
              '2018 ',
            ),
            SizedBox(
              height: 68,
            ),
            headerflag == 0 ? buttonDelete() : buttonUbah(),
            headerflag == 0
                ? SizedBox(
                    height: 30,
                  )
                : buttonBatal(),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: headerflag == 0 ? header() : headerEdit(),
      body: SingleChildScrollView(
        child: content(),
      ),
    );
  }
}

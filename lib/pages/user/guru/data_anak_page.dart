import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class DataAnakPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        elevation: 0.5,
        backgroundColor: s3Color,
        centerTitle: true,
        title: Text(
          'Data Anak',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget listItem() {
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, '/detail-informasi/anak/detail/');
        },
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 18,
            horizontal: 20,
          ),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: mono3Color,
                width: 0.5,
              ),
            ),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Eka Widyawati Eka Widyawati Eka Widyawati Eka Widyawati Eka Widyawati',
                          style: subtitleProfileTextStyle,
                        ),
                        Text(
                          'Bangkok, 2002-02-22',
                          style: labelTextStyle,
                        ),
                      ],
                    ),
                  ),
                  Image.asset(
                    'assets/user/detailinformasi/dataanak/icon_detail_anak.png',
                    width: 20,
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              listItem(),
              listItem(),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: content(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/detail-informasi/anak/tambah/');
        },
        tooltip: 'Tambah Anak',
        child: const Icon(
          Icons.add,
          size: 27.44,
        ),
        backgroundColor: s3Color,
      ),
    );
  }
}

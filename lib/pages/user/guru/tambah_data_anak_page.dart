import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';

class TambahDataAnakPage extends StatefulWidget {
  @override
  _TambahDataAnakPageState createState() => _TambahDataAnakPageState();
}

class _TambahDataAnakPageState extends State<TambahDataAnakPage> {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Tambah Data Anak',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget inputField(String title) {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  TextFormField(
                    enabled: true,
                    readOnly: false,
                    decoration: InputDecoration(
                      hintText: title,
                      contentPadding: EdgeInsets.only(
                        bottom: 8,
                      ),
                      hintStyle: subtitleProfileTextStyle.copyWith(
                        color: mono3Color,
                      ),
                      isCollapsed: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: m1Color,
                          width: 0.5,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget buttonTambah() {
      return Container(
        margin: EdgeInsets.only(
          bottom: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 48,
        ),
        width: double.infinity,
        height: 46,
        child: TextButton(
          onPressed: () {
            setState(() {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                    behavior: SnackBarBehavior.floating,
                    backgroundColor: greenColor,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(8),
                      ),
                    ),
                    content: Text(
                      'Data berhasil diubah',
                      style: headingTextStyle.copyWith(
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.center,
                    )),
              );
            });
          },
          style: TextButton.styleFrom(
              primary: whiteTextStyleColor,
              backgroundColor: s4Color,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          child: Text(
            'Tambah',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
            ),
          ),
        ),
      );
    }

    Widget buttonKembali() {
      return Container(
        margin: EdgeInsets.only(
          bottom: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 48,
        ),
        width: double.infinity,
        height: 46,
        child: TextButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
            });
          },
          style: TextButton.styleFrom(
              side: BorderSide(
                color: mono3Color,
              ),
              backgroundColor: whiteTextStyleColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          child: Text(
            'Kembali',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
              color: mono3Color,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          children: [
            inputField(
              'Nama Anak',
            ),
            inputField(
              'NISN',
            ),
            inputField(
              'Gender',
            ),
            inputField(
              'Tempat Lahir',
            ),
            inputField(
              'Tanggal Lahir',
            ),
            inputField(
              'Jenjang',
            ),
            inputField(
              'Status',
            ),
            inputField(
              'Tahun Masuk',
            ),
            SizedBox(
              height: 68,
            ),
            buttonTambah(),
            buttonKembali(),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: SingleChildScrollView(
        child: content(),
      ),
    );
  }
}

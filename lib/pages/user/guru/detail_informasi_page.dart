import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class DetailInformasiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        elevation: 0.5,
        backgroundColor: s3Color,
        centerTitle: true,
        title: Text(
          'Detail Informasi',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget listItem({
      required String image,
      required String name,
    }) {
      return Container(
        padding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 20,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: mono3Color,
              width: 0.5,
            ),
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/detailinformasi/$image.png',
              width: 20,
              color: name == 'Keluar' ? redColor : wir1Color,
            ),
            SizedBox(
              width: 32,
            ),
            Text(
              name,
              style: subtitleProfileTextStyle.copyWith(
                color: name == 'Keluar' ? redColor : wir1Color,
              ),
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Scaffold(
        backgroundColor: whiteTextStyleColor,
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/detail-informasi/anak');
                  },
                  child: listItem(
                    image: 'icon_anak',
                    name: 'Anak',
                  ),
                ),
                listItem(
                  image: 'icon_beasiswa',
                  name: 'Beasiswa',
                ),
                listItem(
                  image: 'icon_diklat',
                  name: 'Diklat',
                ),
                listItem(
                  image: 'icon_karya_tulis',
                  name: 'Karya Tulis',
                ),
                listItem(
                  image: 'icon_kesejahteraan',
                  name: 'Kesejahteraan',
                ),
                listItem(
                  image: 'icon_tunjangan',
                  name: 'Tunjangan',
                ),
                listItem(
                  image: 'icon_tugas_tambahan',
                  name: 'Tugas Tambahan',
                ),
                listItem(
                  image: 'icon_penghargaan',
                  name: 'Penghargaan',
                ),
                listItem(
                  image: 'icon_nilai_tes',
                  name: 'Nilai Tes',
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/detail-informasi/riwayat');
                  },
                  child: listItem(
                    image: 'icon_riwayat',
                    name: 'Riwayat',
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: header(),
      body: content(),
    );
  }
}

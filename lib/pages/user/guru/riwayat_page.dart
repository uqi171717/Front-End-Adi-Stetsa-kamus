import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class RiwayatPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        elevation: 0.5,
        backgroundColor: s3Color,
        centerTitle: true,
        title: Text(
          'Riwayat',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget listItem({
      required String image,
      required String name,
    }) {
      return Container(
        padding: EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 20,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: mono3Color,
              width: 0.5,
            ),
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/detailinformasi/riwayat/$image.png',
              width: 24,
              color: name == 'Keluar' ? redColor : wir1Color,
            ),
            SizedBox(
              width: 34,
            ),
            Text(
              name,
              style: subtitleProfileTextStyle.copyWith(
                color: name == 'Keluar' ? redColor : wir1Color,
              ),
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              listItem(
                image: 'icon_gaji_berkala',
                name: 'Gaji Berkala',
              ),
              listItem(
                image: 'icon_jabatan_struktural',
                name: 'Jabatan Struktural',
              ),
              listItem(
                image: 'icon_kepangkatan',
                name: 'Kepangkatan',
              ),
              listItem(
                image: 'icon_pendidikan_formal',
                name: 'Pendidikan Formal',
              ),
              listItem(
                image: 'icon_sertifikasi',
                name: 'Sertifikasi',
              ),
              listItem(
                image: 'icon_jabatan_fungsional',
                name: 'Jabatan Fungsional',
              ),
              listItem(
                image: 'icon_karir',
                name: 'Karir',
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: content(),
    );
  }
}

import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class EmailInputPage extends StatefulWidget {
  @override
  _EmailInputPageState createState() => _EmailInputPageState();
}

FocusNode emailFocusNode = new FocusNode();
int isActive = -1;
bool isLoading = false;

class _EmailInputPageState extends State<EmailInputPage> {
  TextEditingController emailController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    SiswaModel siswaModel = authProvider.Siswa;
    GuruModel guruModel = authProvider.Guru;
    RolesModel rolesModel = authProvider.Role;
    KaryawanModel karyawan = authProvider.Karyawan;

    var email = rolesModel.name == 'Siswa'
        ? siswaModel.eMAIL.toString()
        : rolesModel.name == 'Guru'
            ? guruModel.eMAIL.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.eMAIL.toString()
                : '';

    handleEditEmail() async {
      setState(() {
        isLoading = true;
      });
      if (await authProvider.editProfile(
        email: emailController.text,
      )) {
        print('berhasil');
        Navigator.pop(context);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: d1Color,
          content: Text(
            "Gagal Update Data",
            textAlign: TextAlign.center,
          ),
        ));
      }
      setState(() {
        isLoading = false;
      });
    }

    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Ubah profil',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget emailInput() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        width: double.infinity,
        height: 42,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color:
                emailFocusNode.hasFocus || isActive == 0 ? m1Color : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/profil/icon_email.png',
              width: 18,
              color: emailFocusNode.hasFocus || isActive == 0
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                focusNode: emailFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 0;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: email == '' ? 'Email' : email,
                  hintStyle: disablePlaceHolder.copyWith(
                    color: emailFocusNode.hasFocus || isActive == 0
                        ? m1Color
                        : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: emailFocusNode.hasFocus || isActive == 0
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget submitButton() {
      return Container(
        width: double.infinity,
        height: 46,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: s1Color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          onPressed: () {
            setState(() {
              isActive = -1;
            });
            handleEditEmail();
            // Navigator.pushNamedAndRemoveUntil(
            //     context, '/roles', (route) => false);
          },
          child: isLoading == false
              ? Text(
                  'Ubah',
                  style: buttonTextStyle.copyWith(
                    fontWeight: bold,
                  ),
                )
              : Container(
                  width: 14,
                  height: 14,
                  child: CircularProgressIndicator(
                    color: whiteTextStyleColor,
                    strokeWidth: 4,
                  ),
                ),
        ),
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: 45,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 18,
            ),
            Text(
              'Masukkan alamat email yang baru',
              style: subtitleFooterTextStyle.copyWith(
                color: mono2Color,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            emailInput(),
            SizedBox(
              height: 30,
            ),
            submitButton(),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        isActive = -1;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

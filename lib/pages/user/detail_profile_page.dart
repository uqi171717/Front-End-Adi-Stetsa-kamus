import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/orangtua_model.dart';
import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:adistetsa/theme.dart';

class DetailProfilePage extends StatefulWidget {
  @override
  _DetailProfilePageState createState() => _DetailProfilePageState();
}

class _DetailProfilePageState extends State<DetailProfilePage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    SiswaModel siswaModel = authProvider.Siswa;
    GuruModel guruModel = authProvider.Guru;
    RolesModel rolesModel = authProvider.Role;
    OrangTuaModel orangTua = authProvider.OrangTua;
    KaryawanModel karyawan = authProvider.Karyawan;

    var _roleExist = rolesModel.name == 'Siswa' ||
        rolesModel.name == 'Guru' ||
        rolesModel.name == 'Karyawan' ||
        rolesModel.name == 'Staf Kurikulum';

    var _parameternisnipnik = rolesModel.name == 'Siswa'
        ? 'NIS'
        : rolesModel.name == 'Guru' ||
                rolesModel.name == 'Karyawan' ||
                rolesModel.name == 'Staf Kurikulum'
            ? 'NIP'
            : rolesModel.name == 'Orang Tua'
                ? 'NIK'
                : '';

    var _nisnipnik = rolesModel.name == 'Siswa'
        ? '${siswaModel.nIS}'
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? '${guruModel.nIP}'
            : rolesModel.name == 'Orang Tua'
                ? '${orangTua.nIKWALI}'
                : rolesModel.name == 'Karyawan'
                    ? '${karyawan.nIP}'
                    : '';

    var _name = rolesModel.name == 'Siswa'
        ? siswaModel.nAMA.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.nAMALENGKAP.toString()
            : rolesModel.name == 'Orang Tua'
                ? orangTua.nAMAWALI.toString()
                : rolesModel.name == 'Karyawan'
                    ? karyawan.nAMALENGKAP.toString()
                    : '';

    var _email = rolesModel.name == 'Siswa'
        ? siswaModel.eMAIL.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.eMAIL.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.eMAIL.toString()
                : '';

    var _notelp = rolesModel.name == 'Siswa'
        ? siswaModel.hP.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.hP.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.hP.toString()
                : '';

    var _ttl = rolesModel.name == 'Siswa'
        ? siswaModel.tEMPATLAHIR.toString() +
            ', ' +
            siswaModel.tANGGALLAHIR.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.tEMPATLAHIR.toString() +
                ', ' +
                guruModel.tANGGALLAHIR.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.tEMPATLAHIR.toString() +
                    ', ' +
                    karyawan.tANGGALLAHIR.toString()
                : '';

    var _gender = rolesModel.name == 'Siswa'
        ? siswaModel.jENISKELAMIN.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.jENISKELAMIN.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.jENISKELAMIN.toString()
                : '';

    var _agama = rolesModel.name == 'Siswa'
        ? siswaModel.aGAMA.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.aGAMA.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.aGAMA.toString()
                : '';

    var _alamat = rolesModel.name == 'Siswa'
        ? siswaModel.aLAMAT.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.aLAMATTEMPATTINGGAL.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.aLAMATTEMPATTINGGAL.toString()
                : '';

    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Detail Profil',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget profileDetail(String title, String text) {
      return Container(
        width: double.infinity,
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  TextFormField(
                    enabled: false,
                    readOnly: true,
                    decoration: InputDecoration(
                      hintText: text,
                      contentPadding: EdgeInsets.only(
                        bottom: 8,
                      ),
                      hintStyle: subtitleProfileTextStyle,
                      isCollapsed: true,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget email(String title, String text) {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: subtitleFooterTextStyle,
            ),
            SizedBox(
              height: 7,
            ),
            Row(
              children: [
                Expanded(
                  child: Text(
                    text,
                    style: subtitleProfileTextStyle,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/email-input')
                        .then((_) => setState(() {}));
                  },
                  child: Image.asset(
                    'assets/user/profil/pencil_icon.png',
                    width: 20,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Divider(
              thickness: 0.5,
              color: mono3Color,
            ),
          ],
        ),
      );
    }

    Widget phoneNumber(String title, String text) {
      return Container(
        margin: EdgeInsets.only(
          top: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: subtitleFooterTextStyle,
            ),
            SizedBox(
              height: 7,
            ),
            Row(
              children: [
                Expanded(
                  child: Text(
                    text,
                    style: subtitleProfileTextStyle,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/phone-input')
                        .then((_) => setState(() {}));
                  },
                  child: Image.asset(
                    'assets/user/profil/pencil_icon.png',
                    width: 20,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Divider(
              thickness: 0.5,
              color: mono3Color,
            ),
          ],
        ),
      );
    }

    Widget content() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          children: [
            profileDetail('Name', _name),
            profileDetail(
              _parameternisnipnik,
              _nisnipnik,
            ),
            _roleExist
                ? email(
                    'Email',
                    _email,
                  )
                : SizedBox(),
            _roleExist
                ? phoneNumber(
                    'Nomor HP',
                    _notelp,
                  )
                : SizedBox(),
            _roleExist
                ? profileDetail(
                    'TTL',
                    _ttl,
                  )
                : SizedBox(),
            _roleExist
                ? profileDetail(
                    'Gender',
                    _gender,
                  )
                : SizedBox(),
            _roleExist
                ? profileDetail(
                    'Agama',
                    _agama,
                  )
                : SizedBox(),
            _roleExist
                ? profileDetail(
                    'Alamat',
                    _alamat,
                  )
                : SizedBox(),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: SingleChildScrollView(
        child: content(),
      ),
    );
  }
}

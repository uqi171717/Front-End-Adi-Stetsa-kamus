import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/orangtua_model.dart';
import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    SiswaModel siswaModel = authProvider.Siswa;
    GuruModel guruModel = authProvider.Guru;
    RolesModel rolesModel = authProvider.Role;
    OrangTuaModel orangTua = authProvider.OrangTua;
    KaryawanModel karyawan = authProvider.Karyawan;

    var _roleExist = rolesModel.name == 'Siswa' ||
        rolesModel.name == 'Guru' ||
        rolesModel.name == 'Karyawan' ||
        rolesModel.name == 'Staf Kurikulum';

    var _nisnipnik = rolesModel.name == 'Siswa'
        ? 'NIS ${siswaModel.nIS}'
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? 'NIP ${guruModel.nIP}'
            : rolesModel.name == 'Orang Tua'
                ? 'NIK ${orangTua.nIKWALI}'
                : rolesModel.name == 'Karyawan'
                    ? 'NIP ${karyawan.nIP}'
                    : '';

    var _name = rolesModel.name == 'Siswa'
        ? siswaModel.nAMA.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.nAMALENGKAP.toString()
            : rolesModel.name == 'Orang Tua'
                ? orangTua.nAMAWALI.toString()
                : rolesModel.name == 'Karyawan'
                    ? karyawan.nAMALENGKAP.toString()
                    : '';

    var _email = rolesModel.name == 'Siswa'
        ? siswaModel.eMAIL.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.eMAIL.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.eMAIL.toString()
                : '';

    var _notelp = rolesModel.name == 'Siswa'
        ? siswaModel.hP.toString()
        : rolesModel.name == 'Guru' || rolesModel.name == 'Staf Kurikulum'
            ? guruModel.hP.toString()
            : rolesModel.name == 'Karyawan'
                ? karyawan.hP.toString()
                : '';

    PreferredSizeWidget header() {
      return AppBar(
        elevation: 0.5,
        backgroundColor: s3Color,
        centerTitle: true,
        title: Text(
          'Profil',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
        shadowColor: mono5Color,
      );
    }

    Widget dataProfile() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 21,
        ),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: 21,
                bottom: 23,
              ),
              child: Image.asset(
                'assets/user/profil/logo_profile.png',
                width: 130,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                  top: 29,
                  bottom: 26,
                  left: 19,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _nisnipnik,
                      style: labelTextStyle,
                    ),
                    Text(
                      _name,
                      style: subtitleProfileTextStyle,
                    ),
                    _roleExist
                        ? SizedBox(
                            height: 10,
                          )
                        : SizedBox(),
                    _roleExist
                        ? Text(
                            'Email',
                            style: labelTextStyle,
                          )
                        : SizedBox(),
                    _roleExist
                        ? Text(
                            _email,
                            style: subtitleProfileTextStyle,
                          )
                        : SizedBox(),
                    _roleExist
                        ? SizedBox(
                            height: 10,
                          )
                        : SizedBox(),
                    _roleExist
                        ? Text(
                            'Nomor HP',
                            style: labelTextStyle,
                          )
                        : SizedBox(),
                    _roleExist
                        ? Text(
                            _notelp,
                            style: subtitleProfileTextStyle,
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget content({required String image, required String name}) {
      return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: Row(
          children: [
            Image.asset(
              'assets/user/profil/$image.png',
              width: 16,
              color: name == 'Keluar' ? redColor : wir1Color,
            ),
            SizedBox(
              width: 23,
            ),
            Text(
              name,
              style: subtitleProfileTextStyle.copyWith(
                color: name == 'Keluar' ? redColor : wir1Color,
              ),
            ),
          ],
        ),
      );
    }

    confirmLogout() async {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: whiteTextStyleColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 20,
              ),
              width: 305,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: Text(
                          'Notifikasi',
                          style: buttonTextStyle.copyWith(
                            color: wir1Color,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Image.asset(
                            'assets/cancel_button.png',
                            width: 14,
                            height: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 19,
                  ),
                  Image.asset(
                    'assets/icon_warning.png',
                    width: 45,
                    height: 45,
                  ),
                  SizedBox(
                    height: 21,
                  ),
                  Text(
                    'Apakah anda yakin?',
                    style: subtitleProfileTextStyle,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 120,
                        height: 46,
                        child: TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          style: TextButton.styleFrom(
                            backgroundColor: mono3Color,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          child: Text(
                            'Tidak',
                            style: buttonTextStyle.copyWith(
                              fontWeight: bold,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: Container(
                          width: 120,
                          height: 46,
                          child: TextButton(
                            onPressed: () async {
                              SharedPreferences preferences =
                                  await SharedPreferences.getInstance();
                              preferences.clear();
                              Navigator.pushNamedAndRemoveUntil(
                                  context, '/splash-screen', (route) => false);
                            },
                            style: TextButton.styleFrom(
                              backgroundColor: d1Color,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            child: Text(
                              'Ya',
                              style: buttonTextStyle.copyWith(
                                fontWeight: bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      appBar: header(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // header(),
              dataProfile(),
              Divider(
                color: mono3Color,
                thickness: 0.5,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 18,
                  horizontal: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Akun',
                      style: subtitleProfileTextStyle,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    rolesModel.name == 'Karyawan' ||
                            rolesModel.name == 'Orang Tua' ||
                            rolesModel.name == 'Siswa' ||
                            rolesModel.name == 'Guru'
                        ? GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/detail-profile')
                                  .then((_) => setState(() {}));
                            },
                            child: content(
                              image: 'icon_detail_profile',
                              name: 'Detail Profil',
                            ),
                          )
                        : Container(),
                    rolesModel.name == 'Karyawan' || rolesModel.name == 'Guru'
                        ? GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, '/detail-informasi');
                            },
                            child: content(
                              image: 'icon_detail_informasi',
                              name: 'Detail Informasi',
                            ),
                          )
                        : Container(),
                    rolesModel.name == 'Orang Tua' || rolesModel.name == 'Siswa'
                        ? content(
                            image: 'icon_gamepad',
                            name: 'Informasi Poin',
                          )
                        : Container(),
                    content(
                      image: 'icon_pengumuman',
                      name: 'Pengumuman',
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/change-password');
                      },
                      child: content(
                        image: 'icon_ubah_sandi',
                        name: 'Ubah Sandi',
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Lainnya',
                      style: subtitleProfileTextStyle,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    content(
                      image: 'icon_faq',
                      name: 'Bantuan',
                    ),
                    content(
                      image: 'icon_tentang_aplikasi',
                      name: 'Tentang Aplikasi',
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Login',
                      style: subtitleProfileTextStyle,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/roles', (route) => false);
                      },
                      child: content(
                        image: 'icon_ganti_akun',
                        name: 'Ganti Akun',
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        confirmLogout();
                      },
                      child: content(
                        image: 'icon_keluar',
                        name: 'Keluar',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

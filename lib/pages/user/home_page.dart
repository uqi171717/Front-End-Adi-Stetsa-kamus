import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/theme.dart';
import 'package:adistetsa/widget/items_card.dart';
import 'package:adistetsa/widget/profile_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    RolesModel rolesModel = authProvider.Role;

    setState(() {});

    // NOTE: WIDGET ITEM LIST SETIAP ROLE

    Widget itemsStafKurikulum() {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          spacing: 25,
          children: [
            ItemsCard(
              urlIcon: 'calendar_icon.png',
              text: 'Kalender Akademik',
            ),
            GestureDetector(
              onTap: () {
                authProvider.getTahunAjaranFilter(context);
                authProvider.getMataPelajaranFilter(context);
                authProvider.getKelasFilter(context);
                authProvider.getSemesterFilter(context);
                Navigator.pushNamed(context, '/staff/silabus-rpb/');
              },
              child: ItemsCard(
                urlIcon: 'paper_icon.png',
                text: 'Silabus RPB',
              ),
            ),
            GestureDetector(
              onTap: () {
                authProvider.getTahunAjaranFilter(context);
                Navigator.pushNamed(context, '/staff/ktsp/');
              },
              child: ItemsCard(
                urlIcon: 'stamp_icon.png',
                text: 'KTSP',
              ),
            ),
            GestureDetector(
              onTap: () async {
                authProvider.getMataPelajaranFilter(context);
                authProvider.getKelasFilter(context);
                authProvider.getSemesterFilter(context);
                authProvider.getPekanAktif();
                Navigator.pushNamed(context, '/staff/pekan-aktif/');
              },
              child: ItemsCard(
                urlIcon: 'finishtask_icon.png',
                text: 'Jadwal Pekan Aktif',
              ),
            ),
            ItemsCard(
              urlIcon: 'rewind_icon.png',
              text: 'Histori Penilaian',
            ),
            ItemsCard(
              urlIcon: 'list_check_icon.png',
              text: 'Big Data',
            ),
            ItemsCard(
              urlIcon: 'cross_and_check_icon.png',
              text: 'Tata Tertib Sekolah',
            ),
            ItemsCard(
              urlIcon: 'defender_icon.png',
              text: 'Pelanggaran',
            ),
            ItemsCard(
              urlIcon: 'like_dislike_icon.png',
              text: 'Penilaian Sikap',
            ),
            ItemsCard(
              urlIcon: 'fold_stamp_icon.png',
              text: 'Laporan UTS',
            ),
            ItemsCard(
              urlIcon: 'college_hat_icon.png',
              text: 'Ijazah Siswa',
            ),
            ItemsCard(
              urlIcon: 'board_icon.png',
              text: 'E-Raport',
            ),
            ItemsCard(
              urlIcon: 'icon_jurnalbelajar.png',
              text: 'Jurnal Belajar',
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/staff/data-siswa/');
              },
              child: ItemsCard(
                urlIcon: 'data_siswa.png',
                text: 'Data Siswa',
              ),
            ),
          ],
        ),
      );
    }

    Widget itemsGuru() {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          spacing: 25,
          children: [
            ItemsCard(
              urlIcon: 'icon_support_agent.png',
              text: 'Bimbingan Konseling',
            ),
            ItemsCard(
              urlIcon: 'icon_supervisor_account.png',
              text: 'Kesiswaan',
            ),
            ItemsCard(
              urlIcon: 'icon_date_range.png',
              text: 'Kurikulum',
            ),
            ItemsCard(
              urlIcon: 'icon_tata_usaha.png',
              text: 'Tata Usaha',
            ),
            ItemsCard(
              urlIcon: 'icon_upm.png',
              text: 'Unit Penjamin Mutu',
            ),
          ],
        ),
      );
    }

    Widget itemsKaryawan() {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          spacing: 25,
          children: [
            ItemsCard(
              urlIcon: 'icon_date_range.png',
              text: 'Kurikulum',
            ),
            ItemsCard(
              urlIcon: 'icon_tata_usaha.png',
              text: 'Tata Usaha',
            ),
            ItemsCard(
              urlIcon: 'icon_sarana_prasarana.png',
              text: 'Sarana dan Prasarana',
            ),
          ],
        ),
      );
    }

    Widget itemsSiswa() {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          spacing: 25,
          children: [
            ItemsCard(
              urlIcon: 'icon_date_range.png',
              text: 'Kurikulum',
            ),
            ItemsCard(
              urlIcon: 'icon_perpustakaan.png',
              text: 'Perpustakaan',
            ),
            ItemsCard(
              urlIcon: 'icon_support_agent.png',
              text: 'Bimbingan Konseling',
            ),
            ItemsCard(
              urlIcon: 'icon_sarana_prasarana.png',
              text: 'Sarana dan Prasarana',
            ),
            ItemsCard(
              urlIcon: 'icon_supervisor_account.png',
              text: 'Kesiswaan',
            ),
            ItemsCard(
              urlIcon: 'icon_tata_usaha.png',
              text: 'Tata Usaha',
            ),
            ItemsCard(
              urlIcon: 'icon_humas.png',
              text: 'Humas',
            ),
            ItemsCard(
              urlIcon: 'icon_adiwiyata.png',
              text: 'Adiwyata',
            ),
          ],
        ),
      );
    }

    Widget itemsOrangtua() {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          spacing: 25,
          children: [
            ItemsCard(
              urlIcon: 'icon_date_range.png',
              text: 'Kurikulum',
            ),
            ItemsCard(
              urlIcon: 'icon_support_agent.png',
              text: 'Bimbingan Konseling',
            ),
            ItemsCard(
              urlIcon: 'icon_tata_usaha.png',
              text: 'Tata Usaha',
            ),
          ],
        ),
      );
    }

// NOTE: WIDGET ITEM LIST LAINNYA SETIAP ROLE
    Widget contentLainnya({
      required String urlIcon,
      required String title,
      required String text,
    }) {
      return Container(
        margin: EdgeInsets.only(
          bottom: 18,
          left: 24,
          right: 24,
        ),
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 10),
        width: double.infinity,
        decoration: BoxDecoration(
            color: whiteTextStyleColor,
            borderRadius: BorderRadius.circular(
              5,
            ),
            boxShadow: [
              BoxShadow(
                color: wir1Color.withOpacity(0.25),
                spreadRadius: 2,
                blurRadius: 4,
                offset: Offset(0, 2),
              ),
            ]),
        child: Row(
          children: [
            Image.asset(
              'assets/user/$urlIcon.png',
              width: 20,
              height: 20,
            ),
            SizedBox(
              width: 34,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: subtitleFooterTextStyle.copyWith(
                      fontWeight: FontWeight.w600,
                      color: wir1Color,
                    ),
                  ),
                  Text(
                    text,
                    style: subtitleFooterTextStyle.copyWith(
                      color: mono2Color,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Image.asset(
              'assets/icon_arrowRight.png',
              width: 9,
              height: 16,
            ),
          ],
        ),
      );
    }

    Widget listItemGuru() {
      return Column(
        children: [
          contentLainnya(
            urlIcon: 'icon_inventory',
            title: 'Presensi',
            text: 'Dapat digunakan untuk melakukan presensi harian',
          ),
          contentLainnya(
            urlIcon: 'icon_adiwiyata',
            title: 'Adiwiyata',
            text: 'Berisi informasi mengenai kegiatan pada bidang Adiwiyata',
          ),
          contentLainnya(
            urlIcon: 'icon_alumni',
            title: 'Alumni',
            text: 'Menampilkan data alumni SMAN 4 Malang',
          ),
          contentLainnya(
            urlIcon: 'icon_lintasminat',
            title: 'Lintas Minat',
            text: 'Membantu mengolah data mata pelajaran lintas minat siswa',
          ),
          contentLainnya(
            urlIcon: 'icon_sarana_prasarana',
            title: 'Sarana dan Prasarana',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
        ],
      );
    }

    Widget listItemKaryawan() {
      return Column(
        children: [
          contentLainnya(
            urlIcon: 'icon_inventory',
            title: 'Presensi',
            text: 'Dapat digunakan untuk melakukan presensi harian',
          ),
          contentLainnya(
            urlIcon: 'cross_and_check_icon',
            title: 'Tata Tertib',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
          contentLainnya(
            urlIcon: 'icon_ballot',
            title: 'Survey',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
        ],
      );
    }

    Widget listItemOrangtua() {
      return Column(
        children: [
          contentLainnya(
            urlIcon: 'rewind_icon',
            title: 'Riwayat Nilai Siswa',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
          contentLainnya(
            urlIcon: 'icon_clock_dotted',
            title: 'Riwayat Kegiatan Siswa',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
        ],
      );
    }

    Widget listItemSiswa() {
      return Column(
        children: [
          contentLainnya(
            urlIcon: 'icon_inventory',
            title: 'Presensi',
            text: 'Dapat digunakan untuk melakukan presensi harian',
          ),
          contentLainnya(
            urlIcon: 'icon_alumni',
            title: 'Alumni',
            text: 'Menampilkan data alumni SMAN 4 Malang',
          ),
          contentLainnya(
            urlIcon: 'icon_store',
            title: 'Daftar Harga Koperasi',
            text:
                'Menampilkan list harga barang - barang yang berada di koperasi',
          ),
          contentLainnya(
            urlIcon: 'icon_assistant',
            title: 'Prestasi',
            text: 'Membantu mengolah data mata pelajaran lintas minat siswa',
          ),
          contentLainnya(
            urlIcon: 'icon_add_box',
            title: 'Data Kesehatan',
            text: 'Terdapat list data kesehatan individu',
          ),
          contentLainnya(
            urlIcon: 'cross_and_check_icon',
            title: 'Tata Tertib',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
          contentLainnya(
            urlIcon: 'icon_ballot',
            title: 'Survey',
            text: 'Mempermudah peminjaman sarana dan prasarana sekolah',
          ),
        ],
      );
    }

    Widget lainnya() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          rolesModel.name == 'Siswa' ||
                  rolesModel.name == 'Guru' ||
                  rolesModel.name == 'Karyawan' ||
                  rolesModel.name == 'Orang Tua'
              ? Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 24,
                  ),
                  child: Text(
                    'Lainnya',
                    style: heading2TextStyle.copyWith(
                      color: wir1Color,
                    ),
                  ),
                )
              : Container(),
          rolesModel.name == 'Siswa'
              ? listItemSiswa()
              : rolesModel.name == 'Guru'
                  ? listItemGuru()
                  : rolesModel.name == 'Karyawan'
                      ? listItemKaryawan()
                      : rolesModel.name == 'Orang Tua'
                          ? listItemOrangtua()
                          : Container(),
        ],
      );
    }

    Widget content() {
      return Container(
        margin: EdgeInsets.only(
          top: 12,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(
                left: 24,
                right: 24,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "ADISTETSA",
                          style: headingTextStyle.copyWith(
                            fontWeight: bold,
                            color: wir1Color,
                          ),
                        ),
                        Text(
                          'Selamat Datang di Aplikasi Digital SMAN 4 Malang',
                          style: footerTextStyle,
                        ),
                      ],
                    ),
                  ),
                  // NOTE: Hamburger
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/roles');
                    },
                    child: Image.asset(
                      'assets/user/burger_menu_icon.png',
                      width: 18,
                    ),
                  ),
                ],
              ),
            ),
            ProfileCard(),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.only(
                left: 24,
                right: 24,
              ),
              child: Text(
                'MENU',
                style: heading2TextStyle.copyWith(
                  color: wir1Color,
                  fontWeight: regular,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            rolesModel.name == 'Siswa'
                ? itemsSiswa()
                : rolesModel.name == 'Guru'
                    ? itemsGuru()
                    : rolesModel.name == 'Karyawan'
                        ? itemsKaryawan()
                        : rolesModel.name == 'Staf Kurikulum'
                            ? itemsStafKurikulum()
                            : rolesModel.name == 'Orang Tua'
                                ? itemsOrangtua()
                                : Container(),
            lainnya(),
          ],
        ),
      );
    }

    return SafeArea(
      child: ListView(
        children: [
          content(),
        ],
      ),
    );
  }
}

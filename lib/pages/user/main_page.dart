import 'package:adistetsa/pages/user/calendar_page.dart';
import 'package:adistetsa/pages/user/home_page.dart';
import 'package:adistetsa/pages/user/notification_page.dart';
import 'package:adistetsa/pages/user/profile_page.dart';
import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;
  Widget customButtonNav() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 4,
            offset: Offset(0, -1),
            color: wir1Color.withOpacity(
              0.1,
            ),
          ),
        ],
      ),
      child: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (value) {
          print(value);
          setState(() {
            currentIndex = value;
          });
        },
        unselectedLabelStyle: subtitleFooterTextStyle.copyWith(
          fontWeight: FontWeight.w600,
        ),
        selectedLabelStyle: subtitleFooterTextStyle.copyWith(
          fontWeight: FontWeight.w600,
        ),
        backgroundColor: whiteTextStyleColor,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: m1Color,
        items: [
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5.41),
              child: Image.asset(
                'assets/user/bottomnavbar/icon_home.png',
                width: 20,
                color: currentIndex == 0 ? m1Color : mono3Color,
              ),
            ),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 4),
              child: Image.asset(
                'assets/user/bottomnavbar/icon_calendar.png',
                width: 18,
                color: currentIndex == 1 ? m1Color : mono3Color,
              ),
            ),
            label: 'Kalender',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 4),
              child: Image.asset(
                'assets/user/bottomnavbar/icon_notification.png',
                width: 16,
                color: currentIndex == 2 ? m1Color : mono3Color,
              ),
            ),
            label: 'Notifikasi',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset(
                'assets/user/bottomnavbar/icon_profile.png',
                width: 16,
                color: currentIndex == 3 ? m1Color : mono3Color,
              ),
            ),
            label: 'Profil',
          ),
        ],
      ),
    );
  }

  Widget body() {
    switch (currentIndex) {
      case 0:
        return HomePage();
      case 1:
        return CalendarPage();
      case 2:
        return NotificationPage();
      case 3:
        return ProfilePage();
      default:
        return HomePage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteTextStyleColor,
      bottomNavigationBar: customButtonNav(),
      body: body(),
    );
  }
}

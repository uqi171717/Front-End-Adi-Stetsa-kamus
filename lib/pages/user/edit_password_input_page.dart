import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class EditPasswordInputPage extends StatefulWidget {
  @override
  _EditPasswordInputPageState createState() => _EditPasswordInputPageState();
}

FocusNode sandiLamaFocusNode = new FocusNode();
FocusNode sandiBaruFocusNode = new FocusNode();
FocusNode konfirmasiSandiBaruFocusNode = new FocusNode();
int isActive = -1;

class _EditPasswordInputPageState extends State<EditPasswordInputPage> {
  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget header() {
      return AppBar(
        backgroundColor: s3Color,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Ubah Sandi',
          style: heading4TextStyle.copyWith(
            color: whiteTextStyleColor,
            fontWeight: semiBold,
          ),
        ),
      );
    }

    Widget sandiLamaInput() {
      return Container(
        padding: EdgeInsets.all(12),
        width: double.infinity,
        height: 43,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: sandiLamaFocusNode.hasFocus || isActive == 4
                ? m1Color
                : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/profil/icon_lock.png',
              width: 16,
              color: sandiLamaFocusNode.hasFocus || isActive == 4
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                focusNode: sandiLamaFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 0;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: 'Kata Sandi Lama',
                  hintStyle: disablePlaceHolder.copyWith(
                    color: sandiLamaFocusNode.hasFocus || isActive == 4
                        ? m1Color
                        : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: sandiLamaFocusNode.hasFocus || isActive == 4
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget sandiBaruInput() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        width: double.infinity,
        height: 43,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: sandiBaruFocusNode.hasFocus || isActive == 1
                ? m1Color
                : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/profil/icon_lock.png',
              width: 16,
              color: sandiBaruFocusNode.hasFocus || isActive == 1
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                focusNode: sandiBaruFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 0;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: 'Kata sandi baru',
                  hintStyle: disablePlaceHolder.copyWith(
                    color: sandiBaruFocusNode.hasFocus || isActive == 1
                        ? m1Color
                        : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: sandiBaruFocusNode.hasFocus || isActive == 1
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget konfirmasiSandiBaruInput() {
      return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        width: double.infinity,
        height: 43,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: konfirmasiSandiBaruFocusNode.hasFocus || isActive == 2
                ? m1Color
                : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user/profil/icon_lock.png',
              width: 16,
              color: konfirmasiSandiBaruFocusNode.hasFocus || isActive == 2
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                focusNode: konfirmasiSandiBaruFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 0;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: 'Konfirmasi kata sandi baru',
                  hintStyle: disablePlaceHolder.copyWith(
                    color:
                        konfirmasiSandiBaruFocusNode.hasFocus || isActive == 2
                            ? m1Color
                            : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: konfirmasiSandiBaruFocusNode.hasFocus || isActive == 2
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget submitButton() {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        width: double.infinity,
        height: 46,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: s1Color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          onPressed: () {
            setState(() {
              isActive = -1;
            });
            // Navigator.pushNamedAndRemoveUntil(
            //     context, '/roles', (route) => false);
          },
          child: Text(
            'Ubah',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 12,
            ),
            Center(
              child: Column(
                children: [
                  Image.asset(
                    'assets/user/profil/logo_ubah_sandi.png',
                    width: 154,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Ubah Kata Sandi',
                    style: heading4TextStyle.copyWith(
                      color: mono2Color,
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Text(
                    'Masukkan kata sandi baru dibawah',
                    style: subtitleFooterTextStyle.copyWith(
                      color: mono2Color,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 27,
            ),
            Container(
              padding: EdgeInsets.only(
                left: 45,
                right: 46,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Kata Sandi Lama',
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  sandiLamaInput(),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Kata Sandi Baru',
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  sandiBaruInput(),
                  SizedBox(
                    height: 18,
                  ),
                  Text(
                    'Konfirmasi Kata Sandi Baru',
                    style: subtitleFooterTextStyle,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  konfirmasiSandiBaruInput(),
                  SizedBox(
                    height: 41,
                  ),
                  submitButton(),
                ],
              ),
            ),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        isActive = -1;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: whiteTextStyleColor,
        appBar: header(),
        body: content(),
      ),
    );
  }
}

import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/kompetensi_model.dart';
import 'package:adistetsa/models/orangtua_model.dart';
import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class ProfileCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    SiswaModel siswaModel = authProvider.Siswa;
    RolesModel rolesModel = authProvider.Role;
    GuruModel guruModel = authProvider.Guru;
    OrangTuaModel orangTua = authProvider.OrangTua;
    KaryawanModel karyawan = authProvider.Karyawan;

    var _roleExist = rolesModel.name == 'Siswa' ||
        rolesModel.name == 'Karyawan' ||
        rolesModel.name == 'Staf Kurikulum';

    var _nisnipnik = rolesModel.name == 'Siswa'
        ? 'NIS ${siswaModel.nIS}'
        : rolesModel.name == 'Guru'
            ? 'NIP ${guruModel.nIP}'
            : rolesModel.name == 'Orang Tua'
                ? 'NIK ${orangTua.nIKWALI}'
                : rolesModel.name == 'Karyawan'
                    ? 'NIP ${karyawan.nIP}'
                    : rolesModel.name == 'Staf Kurikulum'
                        ? 'NIP ${guruModel.nIP}'
                        : '';

    var _name = rolesModel.name == 'Siswa'
        ? siswaModel.nAMA.toString()
        : rolesModel.name == 'Guru'
            ? guruModel.nAMALENGKAP.toString()
            : rolesModel.name == 'Orang Tua'
                ? orangTua.nAMAWALI.toString()
                : rolesModel.name == 'Karyawan'
                    ? karyawan.nAMALENGKAP.toString()
                    : rolesModel.name == 'Staf Kurikulum'
                        ? guruModel.nAMALENGKAP.toString()
                        : '';

    var _spesialisParameter = rolesModel.name == 'Siswa'
        ? 'Kelas'
        : rolesModel.name == 'Orang Tua'
            ? 'Jumlah Anak'
            : rolesModel.name == 'Guru'
                ? 'Kompetensi'
                : rolesModel.name == 'Karyawan'
                    ? 'Jenis PTK'
                    : rolesModel.name == 'Staf Kurikulum'
                        ? 'Bidang'
                        : '';

    var _spesialis = rolesModel.name == 'Guru'
        ? guruModel.sPESIALISMENANGANI.toString()
        : rolesModel.name == 'Karyawan'
            ? karyawan.jENISPTK.toString()
            : rolesModel.name == 'Staf Kurikulum'
                ? 'Kurikulum'
                : '';

    return Container(
      margin: EdgeInsets.only(
        top: 20,
        left: 24,
        right: 24,
      ),
      padding: EdgeInsets.symmetric(
        vertical: 12,
        horizontal: 24,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 4,
            offset: Offset(0, 4),
            color: Colors.black.withOpacity(
              0.25,
            ),
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            s4Color,
            s3Color,
          ],
        ),
        borderRadius: BorderRadius.circular(
          10,
        ),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                'assets/profile_card_icon.png',
                width: 86.57,
              ),
              Expanded(
                child: Text(
                  rolesModel.name == 'Staf Kurikulum'
                      ? 'Staf'
                      : rolesModel.name,
                  style: buttonTextStyle.copyWith(
                    fontWeight: semiBold,
                    color: mono5Color,
                  ),
                  textAlign: TextAlign.right,
                ),
              ),
            ],
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 14.72,
                      ),
                      Text(
                        _name,
                        style: heading4TextStyle.copyWith(
                          fontWeight: semiBold,
                          color: mono5Color,
                        ),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        _nisnipnik,
                        style: heading2TextStyle.copyWith(
                          fontWeight: regular,
                          color: mono5Color,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        _spesialisParameter,
                        style: subtitleFooterTextStyle.copyWith(
                          color: mono5Color,
                          fontWeight: regular,
                        ),
                      ),
                      rolesModel.name == 'Guru'
                          ? FutureBuilder(
                              future: authProvider.getKompetensiGuru(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.hasData) {
                                  List<KompetensiModel> data = snapshot.data;
                                  return Column(
                                      children: data.map((item) {
                                    return Text(
                                      item.bIDANGSTUDI.toString(),
                                      style: heading4TextStyle.copyWith(
                                        fontWeight: semiBold,
                                        color: mono5Color,
                                      ),
                                    );
                                  }).toList());
                                } else {
                                  return Column(
                                    children: [
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Container(
                                        width: 180,
                                        child: LinearProgressIndicator(
                                          color: Colors.white,
                                          minHeight: 2,
                                          backgroundColor: s1Color,
                                        ),
                                      ),
                                    ],
                                  );
                                }
                              },
                            )
                          : _roleExist
                              ? Text(
                                  _spesialis,
                                  style: heading4TextStyle.copyWith(
                                    fontWeight: semiBold,
                                    color: mono5Color,
                                  ),
                                )
                              : rolesModel.name == 'Orang Tua'
                                  ? FutureBuilder(
                                      future: authProvider.getDataAnak(),
                                      builder: (BuildContext context,
                                          AsyncSnapshot snapshot) {
                                        if (snapshot.hasData) {
                                          List data = snapshot.data.dATAANAK;
                                          return Text(
                                            data.length.toString(),
                                            style: heading4TextStyle.copyWith(
                                              fontWeight: semiBold,
                                              color: mono5Color,
                                            ),
                                          );
                                        } else {
                                          return Column(
                                            children: [
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Container(
                                                width: 180,
                                                child: LinearProgressIndicator(
                                                  color: Colors.white,
                                                  minHeight: 2,
                                                  backgroundColor: s1Color,
                                                ),
                                              ),
                                            ],
                                          );
                                        }
                                      },
                                    )
                                  : SizedBox(),
                    ],
                  ),
                ),
                Image.asset(
                  'assets/adistetsa_profilecard_icon.png',
                  width: 54,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

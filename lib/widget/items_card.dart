import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class ItemsCard extends StatelessWidget {
  final String urlIcon;
  final String text;
  ItemsCard({
    required this.urlIcon,
    required this.text,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 18,
      ),
      padding: EdgeInsets.symmetric(
        vertical: 15,
        horizontal: 8,
      ),
      width: 95,
      height: 95,
      decoration: BoxDecoration(
        color: whiteTextStyleColor,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            blurRadius: 4,
            spreadRadius: 2,
            offset: Offset(0, 2),
            color: wir1Color.withOpacity(0.25),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/user/$urlIcon',
            width: 27,
            height: 30,
          ),
          SizedBox(
            height: 6,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Text(
                text,
                style: subtitleFooterTextStyle.copyWith(
                  fontWeight: semiBold,
                  color: wir1Color,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

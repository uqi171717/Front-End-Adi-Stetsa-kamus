import 'dart:async';

import 'package:adistetsa/theme.dart';
import 'package:flutter/material.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  void initState() {
    Timer(
        const Duration(
          seconds: 3,
        ),
        () => Navigator.pushNamedAndRemoveUntil(
            context, '/main-page', (route) => false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                'assets/bg_splashscreen_kiri.png',
                height: MediaQuery.of(context).size.height,
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Image.asset(
                'assets/bg_splashscreen_kanan.png',
                height: MediaQuery.of(context).size.height,
              ),
            ),
            Center(
              child: CircularProgressIndicator(
                color: s1Color,
              ),
            )
          ],
        ),
      ),
    );
  }
}

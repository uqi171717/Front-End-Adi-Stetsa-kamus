import 'package:adistetsa/providers/auth_providers.dart';
import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

FocusNode usernameFocusNode = new FocusNode();
FocusNode passwordFocusNode = new FocusNode();
int isActive = -1;
bool isLoading = false;

class _LoginPageState extends State<LoginPage> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    handleLogin() async {
      setState(() {
        isLoading = true;
      });
      if (await authProvider.login(
        username: usernameController.text,
        password: passwordController.text,
      )) {
        Navigator.pushNamed(context, '/roles');
      } else {
        passwordController.clear();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: d1Color,
          content: Text(
            "Gagal Login",
            textAlign: TextAlign.center,
          ),
        ));
      }
      setState(() {
        isLoading = false;
      });
    }

    Widget usernameInput() {
      return Container(
        margin: EdgeInsets.only(
          top: 29,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        width: 284,
        height: 43,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: usernameFocusNode.hasFocus || isActive == 0
                ? m1Color
                : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/user_icon.png',
              width: 16,
              color: usernameFocusNode.hasFocus || isActive == 0
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                controller: usernameController,
                focusNode: usernameFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 0;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                // controller: usernameController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Username/NIP/NISN',
                  hintStyle: disablePlaceHolder.copyWith(
                    color: usernameFocusNode.hasFocus || isActive == 0
                        ? m1Color
                        : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: usernameFocusNode.hasFocus || isActive == 0
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget passwordInput() {
      return Container(
        margin: EdgeInsets.only(
          top: 24,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 12,
        ),
        width: 284,
        height: 43,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: passwordFocusNode.hasFocus || isActive == 1
                ? m1Color
                : mono3Color,
            width: 2,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              'assets/lockscreen_icon.png',
              width: 16,
              color: passwordFocusNode.hasFocus || isActive == 1
                  ? m1Color
                  : mono3Color,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: TextFormField(
                controller: passwordController,
                obscureText: true,
                focusNode: passwordFocusNode,
                onTap: () {
                  setState(() {
                    isActive = 1;
                  });
                },
                onEditingComplete: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isActive = -1;
                  });
                },
                decoration: InputDecoration.collapsed(
                  hintText: 'Kata Sandi',
                  hintStyle: disablePlaceHolder.copyWith(
                    color: passwordFocusNode.hasFocus || isActive == 1
                        ? m1Color
                        : mono3Color,
                  ),
                ),
                style: disablePlaceHolder.copyWith(
                  color: passwordFocusNode.hasFocus || isActive == 1
                      ? m1Color
                      : mono3Color,
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget footer() {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.only(
          top: 30,
          bottom: 56,
        ),
        child: Column(
          children: [
            Text(
              'Studium Et Sapientias',
              style: footerTextStyle,
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              'Belajar dan Bijaksana',
              style: subtitleFooterTextStyle,
            ),
          ],
        ),
      );
    }

    Widget loginButton() {
      return Container(
        margin: EdgeInsets.only(
          top: 109,
        ),
        width: 177,
        height: 46,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: s1Color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          onPressed: () {
            setState(() {
              isActive = -1;
            });
            handleLogin();
          },
          child: Text(
            'Masuk',
            style: buttonTextStyle.copyWith(
              fontWeight: bold,
            ),
          ),
        ),
      );
    }

    Widget loadingButton() {
      return Container(
        margin: EdgeInsets.only(
          top: 109,
        ),
        width: 177,
        height: 46,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: s1Color,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
          ),
          onPressed: () {},
          child: Container(
            alignment: Alignment.center,
            width: 14,
            height: 14,
            child: CircularProgressIndicator(
              color: whiteTextStyleColor,
              strokeWidth: 2,
            ),
          ),
        ),
      );
    }

    Widget content() {
      return SingleChildScrollView(
        child: Column(
          children: [
            // NOTE: backicon
            GestureDetector(
              onTap: () {
                setState(() {
                  isActive = -1;
                });
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.topRight,
                margin: EdgeInsets.only(
                  top: 40,
                  right: 20,
                ),
                child: Image.asset(
                  'assets/cancel_button.png',
                  width: 14,
                ),
              ),
            ),
            SizedBox(
              height: 57,
            ),
            Image.asset(
              'assets/logoadistetsa2.png',
              width: 63.51,
            ),
            SizedBox(
              height: 60,
            ),
            Text(
              'Masuk Sekarang',
              style: heading4TextStyle.copyWith(
                fontWeight: regular,
              ),
            ),
            usernameInput(),
            passwordInput(),
            SizedBox(
              height: 12,
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                width: 284,
                child: Text(
                  'Lupa Kata Sandi?',
                  style: footerTextStyle.copyWith(
                    color: m1Color,
                    fontWeight: regular,
                  ),
                  textAlign: TextAlign.right,
                ),
              ),
            ),
            isLoading == false ? loginButton() : loadingButton(),
            footer(),
          ],
        ),
      );
    }

    return WillPopScope(
      onWillPop: () async {
        isActive = -1;
        Navigator.pop(context);
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                height: MediaQuery.of(context).size.height,
                child: Image.asset(
                  'assets/bg_login_kiri.png',
                  // fit: BoxFit.fill,
                ),
              ),
              Container(
                alignment: Alignment.topRight,
                height: MediaQuery.of(context).size.height,
                child: Image.asset(
                  'assets/bg_login_kananatas.png',
                  // fit: BoxFit.fill,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                height: MediaQuery.of(context).size.height,
                child: Image.asset(
                  'assets/bg_login_kanantengah.png',
                  // fit: BoxFit.fill,
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                height: MediaQuery.of(context).size.height,
                child: Image.asset(
                  'assets/bg_login_kananbawah.png',
                  // fit: BoxFit.fill,
                ),
              ),
              content(),
            ],
          ),
        ),
      ),
    );
  }
}

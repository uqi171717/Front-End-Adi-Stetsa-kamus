import 'package:adistetsa/login_page.dart';
import 'package:adistetsa/pages/user/detail_profile_page.dart';
import 'package:adistetsa/pages/user/edit_password_input_page.dart';
import 'package:adistetsa/pages/user/email_input_page.dart';
import 'package:adistetsa/pages/user/guru/data_anak_page.dart';
import 'package:adistetsa/pages/user/guru/detail_anak_page.dart';
import 'package:adistetsa/pages/user/guru/detail_informasi_page.dart';
import 'package:adistetsa/pages/user/guru/riwayat_page.dart';
import 'package:adistetsa/pages/user/guru/tambah_data_anak_page.dart';
import 'package:adistetsa/pages/user/main_page.dart';
import 'package:adistetsa/pages/user/phone_input_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/data-siswa/list_data_siswa_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/data-siswa/detail_data_siswa_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/jadwal-pekan-aktif/detail_pekan_aktif_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/jadwal-pekan-aktif/jadwal_pekan_aktif_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/ktsp/edit_ktsp_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/ktsp/list_ktsp_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/ktsp/tambah_ktsp_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/silabus-rpb/edit_silabus_rpb_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/silabus-rpb/list_silabus_rpb_page.dart';
import 'package:adistetsa/pages/user/staff/kurikulum/silabus-rpb/tambah_silabus_rpb_page.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/roles_page.dart';
import 'package:adistetsa/splash_screen.dart';
import 'package:adistetsa/widget/loading_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main(List<String> args) {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future token() async {
      SharedPreferences pref = await SharedPreferences.getInstance();
      return pref.getString('token');
    }

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case '/login':
              return PageTransition(
                child: LoginPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/roles':
              return PageTransition(
                child: RolesPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/splash-screen':
              return PageTransition(
                child: SplashScreen(),
                type: PageTransitionType.leftToRight,
              );
            case '/main-page':
              return PageTransition(
                child: MainPage(),
                type: PageTransitionType.bottomToTop,
              );
            case '/main-page/back':
              return PageTransition(
                child: MainPage(),
                type: PageTransitionType.leftToRight,
              );
            case '/detail-profile':
              return PageTransition(
                child: DetailProfilePage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/email-input':
              return PageTransition(
                child: EmailInputPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/phone-input':
              return PageTransition(
                child: PhoneInputPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/change-password':
              return PageTransition(
                child: EditPasswordInputPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/detail-informasi':
              return PageTransition(
                child: DetailInformasiPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/detail-informasi/riwayat':
              return PageTransition(
                child: RiwayatPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/detail-informasi/anak':
              return PageTransition(
                child: DataAnakPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/detail-informasi/anak/detail/':
              return PageTransition(
                child: DetailAnakPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/detail-informasi/anak/tambah/':
              return PageTransition(
                child: TambahDataAnakPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/silabus-rpb/':
              return PageTransition(
                child: ListSilabusRpbPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/silabus-rpb/kembali':
              return PageTransition(
                child: ListSilabusRpbPage(),
                type: PageTransitionType.leftToRight,
              );
            case '/staff/silabus-rpb/tambah/':
              return PageTransition(
                child: TambahSilabusRPBPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/silabus-rpb/edit/':
              return PageTransition(
                child: EditSilabusRPBPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/ktsp/':
              return PageTransition(
                child: ListKtspPagePage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/ktsp/back':
              return PageTransition(
                child: ListKtspPagePage(),
                type: PageTransitionType.leftToRight,
              );
            case '/staff/ktsp/tambah/':
              return PageTransition(
                child: TambahKtspPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/ktsp/edit/':
              return PageTransition(
                child: EditKtspPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/pekan-aktif/':
              return PageTransition(
                child: JadwalPekanAktifPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/pekan-aktif/detail-pekan-aktif/':
              return PageTransition(
                child: DetailPekanAktifPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/data-siswa/':
              return PageTransition(
                child: ListDataSiswaPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/staff/data-siswa/detail-data-siswa/':
              return PageTransition(
                child: DetailDataSiswaPage(),
                type: PageTransitionType.rightToLeft,
              );
            case '/loading-page':
              return PageTransition(
                child: LoadingPage(),
                type: PageTransitionType.fade,
              );
          }
        },
        home: FutureBuilder(
          future: token(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return RolesPage();
            } else {
              return SplashScreen();
            }
          },
        ),
      ),
    );
  }
}

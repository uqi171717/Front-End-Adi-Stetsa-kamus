import 'package:adistetsa/models/roles_model.dart';
import 'package:adistetsa/pages/user/home_page.dart';
import 'package:adistetsa/providers/auth_providers.dart';
import 'package:adistetsa/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RolesPage extends StatefulWidget {
  @override
  _RolesPageState createState() => _RolesPageState();
}

class _RolesPageState extends State<RolesPage> {
  bool isLoading = false;
  String name = '';
  int selectedIndex = -1;
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    roleHandler() async {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      var role = preferences.getString('role');
      setState(() {
        isLoading = true;
      });
      if (selectedIndex == -1) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: d1Color,
          content: Text(
            "Pilih akun dulu",
            textAlign: TextAlign.center,
          ),
        ));
      } else {
        if (role == 'Siswa') {
          //NOTE : Ambil Data Disini
          await authProvider.getSiswaProfile();
          //NOTE : Pindah Page Pakai Duration
          Navigator.pushNamedAndRemoveUntil(
              context, '/loading-page', (route) => false);
        } else if (role == 'Karyawan') {
          //NOTE : Ambil Data Disini
          await authProvider.getKaryawanProfile();
          //NOTE : Pindah Page Pakai Duration
          Navigator.pushNamedAndRemoveUntil(
              context, '/loading-page', (route) => false);
        } else if (role == 'Guru') {
          //NOTE : Ambil Data Disini
          await authProvider.getGuruProfile();
          await authProvider.getKompetensiGuru();
          //NOTE : Pindah Page Pakai Duration
          Navigator.pushNamedAndRemoveUntil(
              context, '/loading-page', (route) => false);
        } else if (role == 'Orang Tua') {
          //NOTE : Ambil Data Disini
          await authProvider.getOrangtuaProfile();
          //NOTE : Pindah Page Pakai Duration
          Navigator.pushNamedAndRemoveUntil(
              context, '/loading-page', (route) => false);
        } else if (role == 'Staf Kurikulum') {
          //NOTE : Ambil Data Disini
          await authProvider.getGuruProfile();
          // authProvider.getKompetensiGuru();
          //NOTE : Pindah Page Pakai Duration
          Navigator.pushNamedAndRemoveUntil(
              context, '/loading-page', (route) => false);
        }
      }
      setState(() {
        isLoading = false;
      });
    }

// NOTE: OPTION
    Widget option(int index, RolesModel roles) {
      return GestureDetector(
          onTap: () {
            setState(() {
              authProvider.role = RolesModel(name: roles.name);
              selectedIndex = index;
              Future loadPrefs() async {
                SharedPreferences pref = await SharedPreferences.getInstance();
                return pref.setString('role', roles.name);
              }

              loadPrefs();
            });
          },
          child: Container(
            height: 44,
            padding: EdgeInsets.symmetric(
              horizontal: 24,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: selectedIndex == index ? s1Color : Colors.grey,
                width: 2,
              ),
            ),
            child: Row(
              children: [
                // Text()
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        roles.name,
                        style: footerTextStyle.copyWith(
                            color:
                                selectedIndex == index ? s1Color : Colors.grey,
                            fontWeight: bold),
                      ),
                    ],
                  ),
                ),
                selectedIndex == index
                    ? Image.asset(
                        'assets/coolicon.png',
                        width: 20,
                      )
                    : SizedBox(),
              ],
            ),
          ));
    }

    Widget loadingButton() {
      return TextButton(
        onPressed: () {},
        style: TextButton.styleFrom(
          backgroundColor: m1Color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              8,
            ),
          ),
        ),
        child: Container(
          height: 14,
          width: 14,
          child: CircularProgressIndicator(
            color: whiteTextStyleColor,
            strokeWidth: 4,
          ),
        ),
      );
    }

    Widget footer() {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.only(
          top: 30,
          bottom: 56,
        ),
        child: Column(
          children: [
            Text(
              'Studium Et Sapientias',
              style: footerTextStyle,
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              'Belajar dan Bijaksana',
              style: subtitleFooterTextStyle,
            ),
          ],
        ),
      );
    }

    // NOTE: CONTENT
    Widget content() {
      return SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(
            top: 41,
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 12,
          ),
          child: Column(
            children: [
              Text(
                "Pilih Akun",
                style: headingTextStyle.copyWith(
                  color: wir1Color,
                  fontWeight: bold,
                ),
              ),
              SizedBox(
                height: 43,
              ),
              Image.asset(
                'assets/logoadistetsa2.png',
                width: 63.51,
                height: 125,
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 26,
                  left: 27,
                  right: 27,
                ),
                padding: EdgeInsets.symmetric(horizontal: 18),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(
                      10.0,
                    ),
                  ),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 4,
                      color: Colors.grey,
                      offset: Offset(
                        0,
                        4,
                      ),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 34,
                    ),
                    Center(
                      child: Text(
                        'Silahkan memilih akun Anda',
                        style: buttonTextStyle.copyWith(
                          color: wir1Color,
                          fontWeight: regular,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    FutureBuilder(
                      future: authProvider.getRoles(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          int index = -1;
                          List<RolesModel> data = snapshot.data;
                          return Column(
                              children: data.map((item) {
                            index++;
                            HomePage();
                            return Column(
                              children: [
                                Container(
                                    margin: index >= data.length
                                        ? EdgeInsets.all(0)
                                        : EdgeInsets.only(top: 24),
                                    child: option(index, item)),
                              ],
                            );
                          }).toList());
                        } else {
                          return Center(
                            child: CircularProgressIndicator(
                              color: s1Color,
                            ),
                          );
                        }
                      },
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    Text(
                      '*Anda hanya dapat menggunakan satu akun',
                      style: disablePlaceHolder,
                    ),
                    SizedBox(
                      height: 37,
                    ),
                    Center(
                        child: Container(
                      width: 163,
                      height: 46,
                      child: isLoading == true
                          ? loadingButton()
                          : TextButton(
                              onPressed: roleHandler,
                              style: TextButton.styleFrom(
                                backgroundColor: m1Color,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                    8,
                                  ),
                                ),
                              ),
                              child: Text(
                                'Pilih',
                                style: buttonTextStyle.copyWith(
                                  fontWeight: bold,
                                ),
                              ),
                            ),
                    )),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 71,
              ),
              GestureDetector(
                onTap: () async {
                  SharedPreferences preferences =
                      await SharedPreferences.getInstance();
                  preferences.clear();
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/splash-screen', (route) => false);
                },
                child: Text(
                  'Keluar',
                  style: buttonTextStyle.copyWith(
                    fontWeight: semiBold,
                    color: m1Color,
                  ),
                ),
              ),
              SizedBox(
                height: 34,
              ),
              footer(),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            // NOTE: BACKGROUND KIRI
            Container(
              alignment: Alignment.topLeft,
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                'assets/bg_pilihakun_kiriatas.png',
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                'assets/bg_pilihakun_kiritengah.png',
              ),
            ),
            Container(
              alignment: Alignment.bottomLeft,
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                'assets/bg_pilihakun_kiribawah.png',
              ),
            ),
            // NOTE: BACKGROUND KANAN
            Container(
              alignment: Alignment.bottomRight,
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                'assets/bg_pilihakun_kanan.png',
              ),
            ),

            content(),
          ],
        ),
      ),
    );
  }
}

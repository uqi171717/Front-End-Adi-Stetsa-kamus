import 'dart:convert';
import 'package:adistetsa/models/guru_model.dart';
import 'package:adistetsa/models/karyawan_model.dart';
import 'package:adistetsa/models/ktsp_model.dart';
import 'package:adistetsa/models/minggutidakefektif_model.dart';
import 'package:adistetsa/models/orangtua_model.dart';
import 'package:adistetsa/models/pekanaktif_model.dart';
import 'package:adistetsa/models/mingguefektif_model.dart';
import 'package:adistetsa/models/silabusrpb_model.dart';
import 'package:adistetsa/models/siswa_model.dart';
import 'package:adistetsa/models/token_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthService extends ChangeNotifier {
  String baseUrl = 'https://adistetsa.pythonanywhere.com';

  //Menyimpan json kedalam model
  Future<TokenModel> login({
    String? username,
    String? password,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/login/');
    var headers = {'Content-Type': 'application/json'};
    var body = jsonEncode({
      'username': username,
      'password': password,
    });
    var response = await http.post(url, headers: headers, body: body);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      TokenModel token = TokenModel.fromJson(data);
      token.access = 'Bearer ' + data['access'];
      token.refresh = 'Bearer ' + data['refresh'];
      prefs.setString('token', token.access.toString());

      return token;
    } else {
      throw Exception('Gagal Login');
    }
  }

  Future editProfile({
    String email = '',
    String noHp = '',
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token').toString();
    var role = prefs.getString('role');
    var url = Uri.parse('$baseUrl/profile');
    var headers = {"Content-type": "application/json", "authorization": token};
    var body;
    if (email != '') {
      body = jsonEncode({
        'EMAIL': email,
      });
    } else if (noHp != '') {
      if (role == 'Siswa') {
        body = jsonEncode({
          'HP': noHp,
        });
      } else if (role == 'Guru') {
        body = jsonEncode({
          'HP': noHp,
        });
      } else if (role == 'Karyawan') {
        body = jsonEncode({
          'HP': noHp,
        });
      }
    }

    var response = await http.patch(url, headers: headers, body: body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      if (role == 'Siswa') {
        SiswaModel siswaModel = SiswaModel.fromJson(data);
        return siswaModel;
      } else if (role == 'Guru') {
        GuruModel guruModel = GuruModel.fromJson(data);
        return guruModel;
      } else if (role == 'Karyawan') {
        KaryawanModel karyawan = KaryawanModel.fromJson(data);
        return karyawan;
      }
      return token;
    } else {
      throw Exception('Gagal Login');
    }
  }

  getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/profile');
    var token = prefs.getString("token").toString();
    var role = prefs.getString('role');
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      if (role == 'Siswa') {
        SiswaModel siswaModel = SiswaModel.fromJson(data);
        return siswaModel;
      } else if (role == 'Guru') {
        GuruModel guruModel = GuruModel.fromJson(data);
        return guruModel;
      } else if (role == 'Orang Tua') {
        OrangTuaModel orangTua = OrangTuaModel.fromJson(data);
        return orangTua;
      } else if (role == 'Karyawan') {
        KaryawanModel karyawan = KaryawanModel.fromJson(data);
        return karyawan;
      } else if (role == 'Staf Kurikulum') {
        GuruModel guruModel = GuruModel.fromJson(data);
        return guruModel;
      }
    } else {
      throw Exception('Gagal Login');
    }
  }

  getKTSPID({int? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/ktsp/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      KTSPModel ktspModel = KTSPModel.fromJson(data);
      print(data);
      return ktspModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  geSilabusRPBID({int? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/silabus_rpb/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      SilabusRPBModel silabusRPBModel = SilabusRPBModel.fromJson(data);
      print(data);
      return silabusRPBModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }

  getSilabusRPB(String? urlFilter) async {
    // print(urlFilter);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/silabus_rpb?$urlFilter');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      // print(data);
      List<SilabusRPBModel> silabusRPBModel =
          data.map((item) => SilabusRPBModel.fromJson(item)).toList();
      return silabusRPBModel;
    } else {
      return <SilabusRPBModel>[];
    }
  }

  getPekanAktif() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/jadwal_pekan_aktif');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['results'];
      List<PekanAktifModel> pekanAktif =
          data.map((item) => PekanAktifModel.fromJson(item)).toList();
      notifyListeners();
      print(pekanAktif);
      return pekanAktif;
    } else {
      return <PekanAktifModel>[];
    }
  }

  getMingguEfektif({id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/jadwal_pekan_aktif/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['MINGGU_EFEKTIF'];
      List<MingguEfektifModel> mingguEfektif =
          data.map((item) => MingguEfektifModel.fromJson(item)).toList();
      return mingguEfektif;
    } else {
      return <MingguEfektifModel>[];
    }
  }

  getMingguTidakEfektif({id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/kurikulum/jadwal_pekan_aktif/$id');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['MINGGU_TIDAK_EFEKTIF'];
      List<MingguTidakEfektifModel> mingguEfektif =
          data.map((item) => MingguTidakEfektifModel.fromJson(item)).toList();
      return mingguEfektif;
    } else {
      return <MingguTidakEfektifModel>[];
    }
  }

  getDataSiswaNIS({String? nis}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var url = Uri.parse('$baseUrl/data_siswa/$nis');
    var token = prefs.getString("token").toString();
    var headers = {"Content-type": "application/json", "authorization": token};
    var response = await http.get(url, headers: headers);
    // print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      SiswaModel siswaModel = SiswaModel.fromJson(data);
      // print(nis);
      return siswaModel;
    } else {
      throw Exception('Gagal Mendapatkan Data');
    }
  }
}

import 'package:flutter/material.dart';
import 'package:adistetsa/theme.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget content() {
      return Container(
        width: double.infinity,
        padding: EdgeInsets.only(
          top: 41,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Selamat Datang di ADI STETSA',
              style: headingTextStyle.copyWith(
                color: s1Color,
                fontWeight: bold,
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Aplikasi DIgital SMA Negeri 4 Malang',
              style: heading2TextStyle.copyWith(
                color: s1Color,
                fontWeight: regular,
              ),
            ),
            Expanded(
              child: Image.asset(
                'assets/logoadistetsa.png',
                width: 289,
              ),
            ),
            Container(
              width: 215,
              child: Text(
                'Unggul dalam IMTAQ, IPTEK dan berpijak pada budaya bangsa',
                style: heading2TextStyle.copyWith(
                  color: mono2Color,
                  // fontWeight: regular,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 34,
            ),
            Container(
              width: 289,
              height: 44,
              child: TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/login');
                },
                style: TextButton.styleFrom(
                  backgroundColor: m1Color,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          'Get Started',
                          style: buttonTextStyle.copyWith(
                            fontWeight: bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Image.asset(
                        'assets/go_icon.png',
                        width: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 78,
            ),
            Text(
              'Studium Et Sapientia',
              style: footerTextStyle,
            ),
            SizedBox(
              height: 3,
            ),
            Text(
              'Belajar dan Bijaksana',
              style: subtitleFooterTextStyle,
            ),
            SizedBox(
              height: 56,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Image.asset(
                'assets/bg_splashscreen_kiri.png',
                height: MediaQuery.of(context).size.height,
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Image.asset(
                'assets/bg_splashscreen_kanan.png',
                height: MediaQuery.of(context).size.height,
              ),
            ),
            content(),
          ],
        ),
      ),
    );
  }
}

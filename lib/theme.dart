import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// Coloring
Color whiteTextStyleColor = Color(0xffFFFFFF);
Color m1Color = Color(0xff160E8B);
Color s1Color = Color(0xff101070);
Color s2Color = Color(0xff2A2A84);
Color s3Color = Color(0xff444499);
Color s4Color = Color(0xff5E5EAD);
Color s5Color = Color(0xff7777C2);
Color s6Color = Color(0xff9191D6);
Color s7Color = Color(0xffABABEB);
Color d1Color = Color(0xffE02D2A);
Color warn1Color = Color(0xffFC9E07);
Color i1Color = Color(0xff35CAFC);
Color suc1Color = Color(0xffA6ED3B);
Color wir1Color = Color(0xff000000);
Color mono2Color = Color(0xff666666);
Color mono3Color = Color(0xffB2B2B2);
Color mono4Color = Color(0xffE5E5E5);
Color mono5Color = Color(0xffF2F2F2);
Color redColor = Color(0xffE02D2A);
Color greenColor = Color(0xff2DC133);
Color iconColor = Color(0xff2E3A59);

// Fontweight
FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;

// TextStyle
TextStyle headingTextStyle = GoogleFonts.openSans(
  fontSize: 20,
  color: whiteTextStyleColor,
);

TextStyle heading2TextStyle = GoogleFonts.openSans(
  fontSize: 14,
  color: whiteTextStyleColor,
);

TextStyle buttonTextStyle = GoogleFonts.openSans(
  color: whiteTextStyleColor,
  fontSize: 16,
);

TextStyle footerTextStyle = GoogleFonts.openSans(
  fontSize: 12,
  color: mono2Color,
);

TextStyle subtitleFooterTextStyle = GoogleFonts.openSans(
  fontSize: 10,
  color: mono3Color,
);

TextStyle heading4TextStyle = GoogleFonts.openSans(
  fontSize: 18,
  color: wir1Color,
);

TextStyle disablePlaceHolder = GoogleFonts.openSans(
  fontSize: 12,
  color: mono3Color,
);

TextStyle labelTextStyle = GoogleFonts.openSans(
  fontSize: 10,
  color: mono2Color,
);

TextStyle subtitleProfileTextStyle = GoogleFonts.openSans(
  fontSize: 14,
  color: wir1Color,
);
